/**
 * 
 */
package com.magnetica.server.racks;

import gwt.tools.shared.Util;

import java.util.Iterator;
import java.util.List;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.StructureModule;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            Entity or an extension of it
 */
public class ParallelStructureRack<EntityType extends Entity> extends
	CachedParallelUrlRack<EntityType, StructureModule<EntityType>> {
	boolean cancelled = false;
	private final RunOn runOn;

	public static enum RunOn {
		REQUEST_CHILDREN, RESULT_CHILDREN, REQUEST_RECURSIVE, RESULT_RECURSIVE, REQUEST_RECURSIVE_INCL_ROOT, RESULT_RECURSIVE_INCL_ROOT;

		public boolean result() {
			return this.name().startsWith("RESULT");
		}

		public boolean request() {
			return !result();
		}
	}

	public ParallelStructureRack(Boolean useCache, RunOn runOn, StructureModule<EntityType>... modules) {
		super(useCache, runOn.request() ? ForwardEntityAs.REQUEST : ForwardEntityAs.RESULT, modules);
		this.runOn = runOn;
	}

	/**
	 * Allows to overwrite default cache behaviour (disabling it). Uses default
	 * set of entities to run on: RunOn.REQUEST_CHILDREN
	 * 
	 * @param useCache
	 * @param modules
	 */
	public ParallelStructureRack(Boolean useCache, StructureModule<EntityType>... modules) {
		this(useCache, RunOn.REQUEST_CHILDREN, modules);
	}

	/**
	 * This constructor allows to define on which entities the rack will be
	 * executed but does not override the default cache behavior.
	 * 
	 * @param runOn
	 * @param modules
	 */
	public ParallelStructureRack(RunOn runOn, StructureModule<EntityType>... modules) {
		this(null, runOn, modules);
	}

	/**
	 * Usage of cache is defined by magnetica.shared.Util.useCache(). Uses
	 * default set of entities to run on: RunOn.REQUEST_CHILDREN
	 * 
	 * @param modules
	 */
	public ParallelStructureRack(StructureModule<EntityType>... modules) {
		this(RunOn.REQUEST_CHILDREN, modules);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ParallelRack#extractEntitiesToIterateOver(
	 * com.magnetica.shared.data.Request, com.magnetica.shared.data.Result,
	 * java.util.List)
	 */
	@SuppressWarnings("unchecked") @Override protected void extractEntitiesToIterateOver(Request<EntityType> request,
		Result<EntityType> result, List<EntityType> entities) {

		// gather entities according to onRun definition
		if (Util.isEmpty(entities)) { // if not overwritten, we perform the default extraction: the direct children
			switch (runOn) {
			case REQUEST_CHILDREN:
				entities.addAll((List<EntityType>) request.getEntity());
				break;
			case RESULT_CHILDREN:
				entities.addAll((List<EntityType>) result.getEntity());
				break;
			case REQUEST_RECURSIVE:
				entities.addAll((List<EntityType>) request.getEntity().getAll(true, false));
				break;
			case RESULT_RECURSIVE:
				entities.addAll((List<EntityType>) result.getEntity().getAll(true, false));
				break;
			case REQUEST_RECURSIVE_INCL_ROOT:
				entities.addAll((List<EntityType>) request.getEntity().getAll(true, true));
				break;
			case RESULT_RECURSIVE_INCL_ROOT:
				entities.addAll((List<EntityType>) result.getEntity().getAll(true, true));
				break;
			default:
				assert false;
				entities.addAll((List<EntityType>) request.getEntity());
				break;
			}
		}

		// execute custom filters on extracted entities
		Iterator<EntityType> iterator = entities.iterator();
		while (iterator.hasNext()) {
			EntityType entity = iterator.next();
			if (runForEntity(entity)) {
				prepareEntity(request, entity);
			} else {
				iterator.remove();
			}
		}
		super.extractEntitiesToIterateOver(request, result, entities);
	}

	/**
	 * Overwrite if you want to do something with the Entity before it is
	 * processed by the modules, e.g. setting data
	 * 
	 * @param entity
	 */
	protected void prepareEntity(Request<EntityType> request, EntityType entity) {
	}

	/**
	 * Overwrite this method to filter out entities this rank should not be
	 * executed upon (by returning false)
	 * 
	 * @param entity
	 * @return
	 */
	protected boolean runForEntity(Entity entity) {
		return true;
	}

	@Override protected boolean moduleShouldBeExecutedForEntity(StructureModule<EntityType> module, EntityType entity) {
		return Util.isEmpty(entity);
	};
}
