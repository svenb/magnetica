/**
 * 
 */
package com.magnetica.server.racks;

import gwt.tools.server.dao.ParallelUrlRetriever;
import gwt.tools.server.dao.ParallelUrlRetriever.Retriever;
import gwt.tools.shared.Log;

import java.util.LinkedList;
import java.util.List;

import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.magnetica.server.modules.generic.UsesUrlRetriever;
import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;
import com.magnetica.shared.racks.ModuleRack;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            Entity or an extension of it
 * @param <ModuleType>
 *            Usually a StructureModule or a DataModule or any other extension
 *            of Module
 * 
 */
public abstract class ParallelUrlRack<EntityType extends Entity, ModuleType extends Module<EntityType>> extends
	ModuleRack<EntityType, ModuleType> {

	protected final ForwardEntityAs forwardEntityAs;

	public ParallelUrlRack(ModuleType... modules) {
		this(MagneticaSettings.standardEntityForwarding(), modules);
	}

	/**
	 * @param modules
	 * 
	 */
	public ParallelUrlRack(ForwardEntityAs forwardEntityAs, ModuleType... modules) { // DataModule<EntityType, ?>
		super(modules);
		this.forwardEntityAs = forwardEntityAs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ModuleRack#runModules(com.magnetica.shared
	 * .data.Request, com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override protected final void runModules(final Request<EntityType> request, final Result<EntityType> result,
		final Callback callback) {
		final long start = System.currentTimeMillis();
		//		final List<EntityType> entities = cacheLoadEntities(extractEntitiesToIterateOver(request, resultSoFar));
		final List<EntityType> entities = new LinkedList<EntityType>();
		extractEntitiesToIterateOver(request, result, entities);
		final int[] runningModulesCount = new int[] { 0 };
		LinkedList<Retriever> retrievers = new LinkedList<ParallelUrlRetriever.Retriever>();
		SimpleCallback waitForAllModulesToFinish = new SimpleCallback() {

			int finishedModules = 0;

			@Override public void done(boolean success) {
				if (request.isCancelled()) {
					statistics.cancelled++;
					return;
				}
				if (++finishedModules >= runningModulesCount[0]) {
					statistics.completed++;
					Log.trace(ParallelUrlRack.class,
						"finished " + runningModulesCount[0] + " modules in " + (System.currentTimeMillis() - start)
							+ "ms.");
					processingFinished(request, result);
					callback.success();
					return;
				}
				if (timeIsUp(request)) {
					statistics.timeout++;
					cancel(request, result);
					callback.failure();
				}
			}
		};

		// approach: run all modules for one entity first. goal: we prefer to have a few complete entities than all of them half done.
		runForAllEntities(request, result, entities, waitForAllModulesToFinish, runningModulesCount, retrievers);

		ParallelUrlRetriever.waitForResults(retrievers, new RepeatingCommand() {

			@Override public boolean execute() {
				return !timeIsUp(request);
			}
		});
	}

	/**
	 * Hook for cache: overwrite this method to load entities from cache
	 * 
	 * @param request
	 * @param resultMetaData
	 */
	protected final void runForAllEntities(final Request<EntityType> request, Result<EntityType> result,
		List<EntityType> entities, SimpleCallback waitForAllModulesToFinish, final int[] runningModulesCount,
		LinkedList<Retriever> retrievers) {
		for (EntityType entity : entities) {
			switch (forwardEntityAs) {
			case REQUEST: // cloning request as sub-request to make sure it gets cancelled if needed (request.cancel() will cancel all sub-requests automatically)
				runForEachEntity(entity, runningModulesCount, retrievers, request.cloneAsSubRequest(entity), result,
					waitForAllModulesToFinish);
				break;
			case RESULT:
			default:
				runForEachEntity(entity, runningModulesCount, retrievers, request, new Result<EntityType>(entity,
					result.getMetadata()), waitForAllModulesToFinish);
			}

		}

		if (runningModulesCount[0] == 0) {
			// seems as if none of the modules needed to be run -- nothing to wait for.
			waitForAllModulesToFinish.success();
		}
	}

	/**
	 * Hook for cache: overwrite to load a single entity from cache
	 * 
	 * @param entity
	 * @param runningModulesCount
	 * @param retrievers
	 * @param request
	 * @param result
	 * @param waitForAllModulesToFinish
	 */
	protected void runForEachEntity(EntityType entity, final int[] runningModulesCount,
		LinkedList<Retriever> retrievers, final Request<EntityType> request, Result<EntityType> result,
		SimpleCallback waitForAllModulesToFinish) {
		for (ModuleType module : getModules()) {
			if (moduleShouldBeExecutedForEntity(module, entity)) {
				runModuleOnEntity(module, runningModulesCount, retrievers, request, result, waitForAllModulesToFinish);
			}
		}
	}

	/**
	 * Cache hook: overwrite this method to be informed when an entity gets
	 * actually extended with new data elements.
	 * 
	 * @param module
	 * @param runningModulesCount
	 * @param retrievers
	 * @param request
	 * @param result
	 * @param waitForAllModulesToFinish
	 */
	protected void runModuleOnEntity(ModuleType module, final int[] runningModulesCount,
		LinkedList<Retriever> retrievers, final Request<EntityType> request, Result<EntityType> result,
		SimpleCallback waitForAllModulesToFinish) {
		runningModulesCount[0]++;
		statistics.modulesRun++;
		if (module instanceof UsesUrlRetriever) {
			retrievers.add(((UsesUrlRetriever) module).getUrlRetriever());
		}
		//				module.run(new Request<EntityType>(entity, request.getTimeout()), null, waitForAllModulesToFinish);
		module.run(request, result, waitForAllModulesToFinish);
	}

	protected abstract boolean moduleShouldBeExecutedForEntity(ModuleType module, EntityType entity);

	/**
	 * Allows to filter which entities this rack should be work on, e.g. on
	 * direct ancestors entities or all in the tree etc.
	 * 
	 * @param request
	 * @param result
	 */
	protected abstract void extractEntitiesToIterateOver(Request<EntityType> request, Result<EntityType> result,
		List<EntityType> entities);

	//	private List<EntityType> cacheLoadEntities(List<EntityType> entites) {
	//		// default = do nothing
	//		return entites;
	//	}

	/**
	 * Overwrite this method to do any kind of post processing after all
	 * sub-modules have finished. DON'T FORGET super.processingFinished()!
	 */
	protected void processingFinished(Request<EntityType> request, Result<EntityType> result) {
	}

}
