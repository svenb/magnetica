/**
 * 
 */
package com.magnetica.server.racks;

import java.util.List;

import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.DataModule;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            Entity or an extension of it
 * 
 */
public class ParallelDataRack<EntityType extends Entity> extends
	CachedParallelUrlRack<EntityType, DataModule<EntityType, ?>> {
	boolean cancelled = false;
	private final IterateOver iterateOver;

	public static enum IterateOver {
		ChildEntities, AllSubEntitiesRecursively
	}

	public <T extends DataModule<EntityType, ? extends DataElement>> ParallelDataRack(T... modules) {
		this(null, modules);
	}

	public <T extends DataModule<EntityType, ? extends DataElement>> ParallelDataRack(Boolean useCaching, T... modules) {
		this(useCaching, ForwardEntityAs.RESULT, modules);
	}

	public <T extends DataModule<EntityType, ? extends DataElement>> ParallelDataRack(Boolean useCaching,
		ForwardEntityAs forwardEntityAs, T... modules) {
		this(useCaching, forwardEntityAs, IterateOver.AllSubEntitiesRecursively, modules);
	}

	/**
	 * Switching caching off will overwrite the default. But setting
	 * caching=true will be overwritten by the default
	 * {@link MagneticaSettings#enableCache(boolean)}.
	 * 
	 * @param useCaching
	 * @param modules
	 *            the data modules to be executed
	 */
	public <T extends DataModule<EntityType, ? extends DataElement>> ParallelDataRack(Boolean useCaching,
		ForwardEntityAs forwardEntityAs, IterateOver iterateOver, T... modules) {
		super(useCaching, forwardEntityAs, modules);
		this.iterateOver = iterateOver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.CachedParallelRack#extractEntitiesToIterateOver
	 * (com.magnetica.shared.data.Request, com.magnetica.shared.data.Result,
	 * java.util.List)
	 */
	@SuppressWarnings("unchecked") @Override protected void extractEntitiesToIterateOver(Request<EntityType> request,
		Result<EntityType> result, List<EntityType> entities) {
		entities.addAll((List<EntityType>) result.getEntity().getAll(
			iterateOver.equals(IterateOver.AllSubEntitiesRecursively), false));
		super.extractEntitiesToIterateOver(request, result, entities);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ParallelRack#moduleShouldBeExecutedForEntity
	 * (ModuleType module, EntityType entity)
	 */
	protected boolean moduleShouldBeExecutedForEntity(
	// FIXME method not used - aspect of module.isWorking check should be moved to ParallelRack and called automatically
		com.magnetica.shared.modules.generic.DataModule<EntityType, ?> module, EntityType entity) {
		// do not run if...
		// dataelement exists already or module not working or entity not suitable or a dependency missing
		return !(entity.getData(module.providesDataElement()) != null || !module.isWorking()
			|| !module.suitableEntityTypes().contains(entity.getDataType()) || !entity.getDataElementTypes()
			.containsAll(module.dependsOnDataElements()));
	}

}
