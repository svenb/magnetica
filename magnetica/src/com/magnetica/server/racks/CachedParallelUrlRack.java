/**
 * 
 */
package com.magnetica.server.racks;

import gwt.tools.server.dao.ParallelUrlRetriever.Retriever;
import gwt.tools.shared.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.magnetica.shared.CacheMixin;
import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.Util;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Module;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            Entity or an extension of it
 * @param <ModuleType>
 *            Usually a StructureModule or a DataModule or any other extension
 *            of Module
 * 
 */
public abstract class CachedParallelUrlRack<EntityType extends Entity, ModuleType extends Module<EntityType>> extends
	ParallelUrlRack<EntityType, ModuleType> {

	//	private final Cache cache;
	private LinkedList<EntityType> entitiesToBeCached = new LinkedList<EntityType>();
	private final CacheMixin cache = new CacheMixin();

	/**
	 * Switching caching off will overwrite the default. But setting
	 * caching=true will be overwritten by the default
	 * {@link MagneticaSettings#enableCache(boolean)}.
	 * 
	 * @param useCaching
	 * @param modules
	 *            this rack should execute
	 * @param forwardEntityAs
	 */
	public CachedParallelUrlRack(Boolean useCaching, ForwardEntityAs forwardEntityAs, ModuleType... modules) {
		super(forwardEntityAs, modules);
		if (useCaching != null) {
			cache.use(useCaching);
		}
	}

	public CachedParallelUrlRack(ForwardEntityAs forwardEntityAs, ModuleType... modules) {
		this(null, forwardEntityAs, modules);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ParallelStructureRack#extractEntitiesToIterateOver
	 * (com.magnetica.shared.data.Request, com.magnetica.shared.data.Result)
	 */
	@SuppressWarnings("unchecked") @Override protected void extractEntitiesToIterateOver(Request<EntityType> request,
		Result<EntityType> result, List<EntityType> entities) {
		if (entities.size() > 0 && cache.use()) {

			// ask cache for the fully extended entities
			LinkedList<Object> cacheKeys = new LinkedList<Object>();
			Class<EntityType> entityClass = (Class<EntityType>) entities.get(0).getClass();
			for (EntityType entity : entities) {
				Object cacheKey = getCacheKey(entity, request, result);
				if (cacheKey != null) {
					cacheKeys.add(cacheKey);
				}
			}
			if (Util.notEmpty(cacheKeys)) {
				Map<Object, EntityType> cachedEntities = cache.get().getAll(cacheKeys, entityClass);
				// put new entities, thus the link get lost as the entities in the entities list are pointing to the real once in the result.getEntity() tree.
				// thus overwriting the entity in the entities list does not update the tree, instead, the link gets lost
				//				List<EntityType> orgEntities = new LinkedList<EntityType>(entities);
				//				entities.clear();
				//				entities.addAll(new LinkedList<EntityType>(Util.union(cachedEntities.values(), orgEntities)));
				// instead, we copy over the cached data now. shouldn't be to much effort as it is just replacing pointers internally.
				int index;
				for (EntityType cached : cachedEntities.values()) {
					if ((index = entities.indexOf(cached)) > -1) {
						entities.get(index).adaptTo(cached);
					}
				}
				Log.debug(
					this,
					"extractEntitiesToIterateOver: got #cacheEntities cached of #entities total entities, continue loading...",
					cachedEntities.size(), entities.size());
			}
		}
	}

	/**
	 * Overwrite this method if your procedure of loading the right entity from
	 * cache does not depend on the entity's ID (only).
	 * 
	 * @param entity
	 * @param request
	 * @param result
	 * @return unique key
	 */
	protected Object getCacheKey(Entity entity, Request<EntityType> request, Result<EntityType> result) {
		return entity.getId();
	}

	protected void runModuleOnEntity(ModuleType module, int[] runningModulesCount,
		java.util.LinkedList<Retriever> retrievers, com.magnetica.shared.data.Request<EntityType> request,
		com.magnetica.shared.data.Result<EntityType> result, SimpleCallback waitForAllModulesToFinish) {
		switch (forwardEntityAs) {
		case RESULT:
			entitiesToBeCached.add(result.getEntity());
			break;
		case REQUEST:
			entitiesToBeCached.add(request.getEntity());
			break;
		}
		super.runModuleOnEntity(module, runningModulesCount, retrievers, request, result, waitForAllModulesToFinish);
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.racks.ParallelDataRack#processingFinished()
	 */
	@Override protected synchronized void processingFinished(Request<EntityType> request, Result<EntityType> result) {
		storeResults(request, result);
		Log.trace(this, ".processingFinished: done.");
		super.processingFinished(request, result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#cancel(com.magnetica.shared.data.
	 * Request, com.magnetica.shared.data.Result)
	 */
	@Override public void cancel(Request<EntityType> request, Result<EntityType> result) {
		// TODO take note about those entities, that actually have been updated, e.g. by another hook that is called each time result where retrieved successfully
		storeResults(request, result);
		super.cancel(request, result);
	}

	private void storeResults(Request<EntityType> request, Result<EntityType> result) {
		if (entitiesToBeCached.size() > 0 && cache.use()) {
			Log.debug(this, ".processingFinished: will add #toBeCached new/extended entities to the cache...",
				entitiesToBeCached.size());
			LinkedList<EntityType> toCache = new LinkedList<EntityType>(entitiesToBeCached);
			entitiesToBeCached.clear();
			Map<Object, Object> cacheMap = new HashMap<Object, Object>();
			for (EntityType entity : toCache) {
				cacheMap.put(getCacheKey(entity, request, result), entity);
			}
			cache.get().putAll(cacheMap);
		}
	}
}
