/**
 * 
 */
package com.magnetica.server;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.modules.generic.BasicModule;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class BasicServerModule<EntityType extends Entity> extends BasicModule<EntityType> {
}
