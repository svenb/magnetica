package com.magnetica.server;

import gwt.tools.server.dao.ParallelUrlRetriever;
import gwt.tools.server.dao.ParallelUrlRetriever.Statistics;
import gwt.tools.server.dao.ServerCache;
import gwt.tools.shared.Log;
import gwt.tools.shared.Log.FormatOnDemand;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.magnetica.client.MagneticaService;
import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.data.ServerResult;
import com.magnetica.shared.modules.generic.BasicModule;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;
import com.magnetica.shared.racks.ModuleRack;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial") public abstract class MagneticaServiceImpl<EntityType extends Entity> extends
	RemoteServiceServlet implements MagneticaService<EntityType> {

	static {
		MagneticaSettings.setCache(ServerCache.getInstance());
	}

	//	@ServerRootModule private static Module<?> rootModule;
	private final Module<EntityType> rootModule;
	private final Class<DataElement>[] requiredDataElements;

	/**
	 * Initializes magnetica and sets the module that should be called when
	 * requests arrive at server-side. If you give a list of required data
	 * elements, magnetica can automatically evaluate the quality of the
	 * produced results.
	 */
	public MagneticaServiceImpl(Module<EntityType> rootModule, Class<DataElement>... requiredDataElements) {
		this.rootModule = rootModule;
		this.requiredDataElements = requiredDataElements;
		if (MagneticaSettings.clearCacheOnStartUp() && MagneticaSettings.useCache()) {
			clearCache();
		}
		Log.debug(this, "init: rootModule=" + rootModule + "; settings=" + MagneticaSettings.toStr());
	}

	protected void clearCache() {
		MagneticaSettings.getCache().clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.client.MagneticaService#run(com.magnetica.shared.data.Request
	 * )
	 */
	@Override public ServerResult<EntityType> run(Request<EntityType> request) {
		return run(request, new Result<EntityType>(null));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.client.MagneticaService#run(com.magnetica.shared.data.Request
	 * )
	 */
	@Override public ServerResult<EntityType> run(Request<EntityType> request, Result<EntityType> result) {
		Log.trace(this, "MagenticaService.run: request", request);
		Statistics initialUrlRetrieverStatistics = ParallelUrlRetriever.getStatistics();
		com.magnetica.shared.racks.ModuleRack.Statistics initialRackStatistics = ModuleRack.getStatistics();
		gwt.tools.shared.CommonCache.Statistics initialCacheStatistics = gwt.tools.shared.CommonCache.getStatistics();
		//		return new ServerResult(null, true);
		long start = System.currentTimeMillis();
		if (request.hasTimeout()) {
			// set a real timestamp now.
			Log.debug(this, "MagneticaService.run: setting timeout: delta=" + request.getTimeout() + "; currentTime="
				+ System.currentTimeMillis());
			request.setTimeout(request.getTimeout() + System.currentTimeMillis());
		}
		assert rootModule != null : "No server-side root module has been defined! A root module can be defined by calling "
			+ BasicModule.class.getName() + "'s contructor with (rootModule=true).";
		final int done[] = new int[] { -1 };
		Log.debug(this, "MagenticaService.run: executing root module (" + rootModule.getClass() + ")...");
		rootModule.run(request, result, new Callback() {

			@Override public void success() {
				Log.debug(this, "MagenticaService.run: rootModule.success");
				done[0] = 1;
			}

			@Override public void failure() {
				Log.debug(this, "MagenticaService.run: rootModule.failure");
				done[0] = 2;
			}
		});
		int interruptCount = 0;
		while (done[0] < 0 && !timeIsUp(request)) {
			Log.debug(this, "MagenticaService.run: waiting for request", request);
			try {
				// do something useful while waiting: run UrlFetch requests
				if (ParallelUrlRetriever.hasRunningRequests()) {
					ParallelUrlRetriever.run();
				} else {
					Thread.sleep(500);
				}
			} catch (InterruptedException e) {
				Log.warn(this, "MagenticaService.run: waiting for request - interupt exception!");
				if (interruptCount++ > 9) {
					Log.warn(this,
						"MagenticaService.run: waiting for request: to many interupt exceptions, will stop waiting now.");
					break;
				}
			}
		}
		printStats(request, result, start, done[0], initialUrlRetrieverStatistics, initialRackStatistics,
			initialCacheStatistics);
		return new ServerResult(result, done[0] == 1);
	}

	/**
	 * @param entity
	 * @param initialCacheStatistics
	 * @param initialRackStatistics
	 * @param initialUrlRetrieverStatistics
	 * @param initialUrlRetrieverStatistics
	 * @param done
	 * @param start
	 * 
	 */
	private void printStats(final Request<EntityType> request, final Result<EntityType> result, final long startTime,
		final int resultStatus, final Statistics initialUrlRetrieverStatistics,
		final com.magnetica.shared.racks.ModuleRack.Statistics initialRackStatistics,
		final gwt.tools.shared.CommonCache.Statistics initialCacheStatistics) {

		final long startStatsRendering = System.currentTimeMillis();
		Log.log(this, new FormatOnDemand() {

			@Override public String format() {

				// calculate timing first to not measure time required for rendering stats
				final long timing = startStatsRendering - startTime;

				StringBuilder b = new StringBuilder();
				b.append("\n# magnetica call statistics: ");

				// request recap
				b.append("\n# Request: ");
				b.append(request);

				// over all status
				b.append("\n# Result: status=");
				b.append(((resultStatus == 0) ? "timeout" : ((resultStatus == 1) ? "success" : "failure")));
				b.append("; data=");
				b.append(result);

				// url retriever stats
				Statistics statistics = ParallelUrlRetriever.getStatistics();
				b.append("\n# UrlRetieverStats: ∆#urls added=");
				b.append((statistics.urlsAdded - initialUrlRetrieverStatistics.urlsAdded));
				b.append("; ∆#urls=");
				b.append((statistics.urlsRetrieved - initialUrlRetrieverStatistics.urlsRetrieved));
				b.append("; ∆#bytes=");
				b.append(statistics.bytesLoaded - initialUrlRetrieverStatistics.bytesLoaded);
				b.append("; #urls added=");
				b.append(statistics.urlsAdded);
				b.append("; #urls=");
				b.append(statistics.urlsRetrieved);
				b.append("; #bytes=");
				b.append(statistics.bytesLoaded);

				// data completeness
				Entity entity = result.getEntity();
				b.append("\n# Result quality: ");
				if (entity != null) {
					b.append("data completeness=");
					b.append(entity.getDataCompleteness(requiredDataElements));
					b.append("; #entities=");
					b.append(entity.getAll(true, true).size());
				} else {
					b.append("root entity == null");
				}

				// timing
				b.append("\n# Timing: ");
				b.append(timing);
				b.append("ms");

				// module calls
				com.magnetica.shared.racks.ModuleRack.Statistics rackStats = ModuleRack.getStatistics();
				b.append("\n# Rack stats: #∆module runs=");
				b.append(rackStats.modulesRun - initialRackStatistics.modulesRun);
				b.append("; #∆completed=");
				b.append(rackStats.completed - initialRackStatistics.completed);
				b.append("; #∆timeouts=");
				b.append(rackStats.timeout - initialRackStatistics.timeout);
				b.append("; #∆cancelled=");
				b.append(rackStats.cancelled - initialRackStatistics.cancelled);
				b.append("\n#             #module runs=");
				b.append(rackStats.modulesRun);
				b.append("; #completed=");
				b.append(rackStats.completed);
				b.append("; #timeouts=");
				b.append(rackStats.timeout);
				b.append("; #cancelled=");
				b.append(rackStats.cancelled);

				// cache IO
				if (MagneticaSettings.useCache()) {
					gwt.tools.shared.CommonCache.Statistics cacheStats = ServerCache.getStatistics();
					b.append("\n# Cache stats: #∆gets=");
					b.append(cacheStats.gets - initialCacheStatistics.gets);
					b.append("; #∆multiGets=");
					b.append(cacheStats.multiGets - initialCacheStatistics.multiGets);
					b.append("; #∆hits=");
					b.append(cacheStats.hits - initialCacheStatistics.hits);
					b.append("; #∆invalidGets=");
					b.append(cacheStats.invalidGets - initialCacheStatistics.invalidGets);
					b.append("; #∆puts=");
					b.append(cacheStats.puts - initialCacheStatistics.puts);
					b.append("; #∆multiPuts=");
					b.append(cacheStats.multiPuts - initialCacheStatistics.multiPuts);
					b.append("; #∆invalidPuts=");
					b.append(cacheStats.invalidPuts - initialCacheStatistics.invalidPuts);
					b.append("\n#              #gets=");
					b.append(cacheStats.gets);
					b.append("; #multiGets=");
					b.append(cacheStats.multiGets);
					b.append("; #hits=");
					b.append(cacheStats.hits);
					b.append("; #invalidGets=");
					b.append(cacheStats.invalidGets);
					b.append("; #puts=");
					b.append(cacheStats.puts);
					b.append("; #multiPuts=");
					b.append(cacheStats.multiPuts);
					b.append("; #invalidPuts=");
					b.append(cacheStats.invalidPuts);
				} else {
					b.append("\n# Cache stats: cache deactived via Util.");
				}

				return b.toString();
			}
		});
		Log.debug(this, ".printStats: time required", System.currentTimeMillis() - startStatsRendering);
	}

	/**
	 * @param request
	 * @return
	 */
	private boolean timeIsUp(Request request) {
		if (request.getTimeout() <= 0) {
			return false;
		}
		if (request.getTimeout() + 2000 < System.currentTimeMillis()) {
			Log.debug(this, "MagneticaServiceImpl.checkForTimeout: timeout (" + request.getTimeout()
				+ ") while running root module. current time = " + System.currentTimeMillis());
			return true;
		}
		return false;
	}
	//	public static void setRootModule(Module module) {
	//		rootModule = module;
	//	}
}
