/**
 * 
 */
package com.magnetica.server.modules;

import gwt.tools.shared.Log;
import gwt.tools.shared.Util;

import com.magnetica.server.BasicServerModule;
import com.magnetica.shared.CacheMixin;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class EarlyCacheModule<EntityType extends Entity> extends BasicServerModule<EntityType> {

	private CacheMixin cache = new CacheMixin();

	private final Module<EntityType> moduleToBeCached;

	public EarlyCacheModule(Module<EntityType> moduleToBeCached) {
		assert moduleToBeCached != null;
		this.moduleToBeCached = moduleToBeCached;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.generic.Module#run(com.magnetica.shared.
	 * data.Request, com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.generic.Callback)
	 */
	@Override public void run(final Request<EntityType> request, final Result<EntityType> result,
		final Callback callback) {
		if (cache.use() && request.isCacheable()) {
			final String cacheKey = "ec" + request.getCacheKey();
			Object resultEntity = cache.get().get(cacheKey);
			Log.debug(this, ".run: checking cache; key", cacheKey);
			if (resultEntity != null) {
				result.setEntity((EntityType) resultEntity);
				Log.debug(this, ".run: #childEntities", Util.size(result.getEntity()));
				callback.success();
				return;
			}

			moduleToBeCached.run(request, result, new Callback() {

				@Override public void success() {
					cache.get().put(cacheKey, result.getEntity());
					Log.debug(this, ".run: caching for next time: key, #childEntities", cacheKey,
						Util.size(result.getEntity()));
					callback.success();
				}

				@Override public void failure() {
					callback.failure();
				}
			});
		} else {
			Log.debug(this, ".run: by-passing cache", cache.use(), request.isCacheable());
			moduleToBeCached.run(request, result, callback);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.generic.BasicModule#testModule(com.magnetica
	 * .shared.SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		callback.success();
	}

}
