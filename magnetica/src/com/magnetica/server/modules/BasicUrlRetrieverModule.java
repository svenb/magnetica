/**
 * 
 */
package com.magnetica.server.modules;

import gwt.tools.server.dao.ParallelUrlRetriever;
import gwt.tools.server.dao.ParallelUrlRetriever.Retriever;
import gwt.tools.shared.Constants;
import gwt.tools.shared.Log;
import gwt.tools.shared.Util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.server.modules.generic.UsesUrlRetriever;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.modules.generic.BasicModule;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class BasicUrlRetrieverModule<EntityType extends Entity> extends BasicModule<EntityType> implements
	UsesUrlRetriever {

	protected final ParallelUrlRetriever.Retriever urlRetriever = new ParallelUrlRetriever.Retriever();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.UsesUrlRetriever#getUrlRetriever()
	 */
	@Override public Retriever getUrlRetriever() {
		return urlRetriever;
	}

	protected void loadUrl(String url, AsyncCallback<byte[]> callback) throws MalformedURLException {
		urlRetriever.addUrl(url, callback);
	}

	protected void loadUrl(String urlString, long timeout, String requiredContentType, AsyncCallback<byte[]> callback)
		throws MalformedURLException {
		loadUrl(new URL(urlString), timeout, requiredContentType, callback);
	}

	/**
	 * 
	 * 
	 * @param url
	 * @param timeout
	 *            0 for no timeout or timeout in ms
	 * @param requiredContentType
	 * @param callback
	 */
	protected void loadUrl(URL url, long timeout, String requiredContentType, AsyncCallback<byte[]> callback) {
		if (timeout > 0 && timeout < 200) {
			// not worth trying
			Log.warn(SimpleUrlRetrieverModule.class, ".run: not tried because time left is too short. URL", url);
			callback.onFailure(new IOException(
				"BasicUrlRetrieverModule.loadUrl: timeout is less than 200ms. Not worth trying..."));
			return;
		}
		HTTPRequest httpRequest = null;
		String urlString = url.toString();
		if (urlString.length() > Constants.MAX_URL_LENGTH) {
			if (urlString.indexOf('?') < 0) {
				Log.severe(
					this,
					"loadUrl: URL is longer than 2000 characters but there are no parameters that could be sent via POST (there is no \"?\" in the URL)! Thus, this URL can not be loaded. URL",
					urlString);
				//				callback.onSuccess(new byte[0]);
				// There is no data that can be loaded in a retry using this URL, thus success aka we did all we could.
				callback.onSuccess(null);
			}
			Log.warn(this, "loadUrl: URL longer than 2000 characters. Trying POST mode...");
			String[] urlSplit = urlString.split("\\?");
			assert urlSplit.length == 2 : urlSplit;
			try {
				httpRequest = new HTTPRequest(new URL(urlSplit[0]), HTTPMethod.POST);
				httpRequest.setPayload(urlSplit[1].getBytes(Constants.UTF8));
			} catch (Throwable t) {
				Log.warn(this, "loadUrl: URL longer than 2000 characters. Failed to convert to POST request! error", t);
			}
		}
		if (httpRequest == null) {
			httpRequest = new HTTPRequest(url);
		}
		httpRequest.getFetchOptions().disallowTruncate().doNotValidateCertificate().followRedirects();
		if (timeout > 0) {
			httpRequest.getFetchOptions().setDeadline(timeout / 1000d);
		}
		if (Util.notEmpty(requiredContentType)) {
			httpRequest.setHeader(new HTTPHeader("Accept", requiredContentType));
		}
		urlRetriever.addRequest(httpRequest, callback);
	}

	protected boolean hasDataToLoad() {
		return urlRetriever.isRunning();
	}
}
