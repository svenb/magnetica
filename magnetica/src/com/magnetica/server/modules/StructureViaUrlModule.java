/**
 * 
 */
package com.magnetica.server.modules;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.modules.generic.StructureModule;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public abstract class StructureViaUrlModule<EntityType extends Entity> extends SimpleUrlRetrieverModule<EntityType>
	implements StructureModule<EntityType> {

	public StructureViaUrlModule() {
		super();
	}

	public StructureViaUrlModule(String acceptsContentType) {
		super(acceptsContentType);
	}

}
