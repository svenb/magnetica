/**
 * 
 */
package com.magnetica.server.modules.generic;

import gwt.tools.server.dao.ParallelUrlRetriever;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface UsesUrlRetriever {
	ParallelUrlRetriever.Retriever getUrlRetriever();
}
