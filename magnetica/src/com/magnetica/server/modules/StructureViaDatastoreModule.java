/**
 * 
 */
package com.magnetica.server.modules;

import gwt.tools.server.dao.RawDatastoreDao;

import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.BasicStructureModule;
import com.magnetica.shared.modules.generic.Callback;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class StructureViaDatastoreModule<EntityType extends Entity> extends BasicStructureModule<EntityType> {

	RawDatastoreDao datastore;
	private final boolean createOnDemand;

	/**
	 * @param namespace
	 * @param createOnDemand
	 *            Override {@link #create(Request)} method to implement the
	 *            automatic entity creation
	 * 
	 */
	public StructureViaDatastoreModule(String namespace, boolean createOnDemand) {
		this.createOnDemand = createOnDemand;
		datastore = new RawDatastoreDao(namespace, RawDatastoreDao.LIVE_FOREVER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public void run(Request<EntityType> request, Result<EntityType> result, Callback callback) {
		result.setEntity(load(getDataSetDefinitionId(request), request));
		callback.success();
	}

	protected abstract String getDataSetDefinitionId(Request<EntityType> request);

	protected EntityType load(String id, Request<EntityType> request) {
		Object entity = datastore.get(id);
		if (entity != null) {
			assert entity instanceof Entity : entity;
		} else {
			if (createOnDemand && (entity = create(request)) != null) {
				datastore.put(id, entity, true);
			}
		}
		return (EntityType) entity;
	}

	protected Entity create(Request<EntityType> request) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModule#testModule(com.magnetica.shared
	 * .SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		if (datastore != null) {
			callback.success();
		} else {
			callback.failure();
		}
	}
}
