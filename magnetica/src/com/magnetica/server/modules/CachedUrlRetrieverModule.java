/**
 * 
 */
package com.magnetica.server.modules;

import java.net.URL;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.shared.CacheMixin;
import com.magnetica.shared.data.Entity;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class CachedUrlRetrieverModule<EntityType extends Entity> extends BasicUrlRetrieverModule<EntityType> {

	private final CacheMixin cache = new CacheMixin();

	/**
	 * 
	 */
	public CachedUrlRetrieverModule() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicUrlRetrieverModule#loadUrl(java.net
	 * .URL, long, java.lang.String,
	 * com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override protected void loadUrl(URL url, long timeout, String requiredContentType,
		final AsyncCallback<byte[]> callback) {
		if (cache.use()) {
			final String cacheKey = url.toExternalForm() + ";requiredContentType=" + requiredContentType;
			byte[] result = cache.get().get(cacheKey, byte[].class);
			if (result != null) {
				callback.onSuccess(result);
				return;
			}
			super.loadUrl(url, timeout, requiredContentType, new AsyncCallback<byte[]>() {

				@Override public void onSuccess(byte[] result) {
					cache.get().put(cacheKey, result);
					callback.onSuccess(result);
				}

				@Override public void onFailure(Throwable caught) {
					callback.onFailure(caught);
				}
			});
		} else {
			super.loadUrl(url, timeout, requiredContentType, callback);
		}
	}

}
