/**
 * 
 */
package com.magnetica.server.modules;

import java.util.LinkedList;
import java.util.List;

import com.magnetica.shared.CacheMixin;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.DataCallback;

/**
 * If you plan to use your module in a rack you should activate caching for the
 * rack and not use a cached module for performance reasons as the rack can
 * handle caching for all modules at once.
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class CachedStructureModule<EntityType extends Entity> extends BasicUrlRetrieverModule<EntityType> {

	private CacheMixin cache = new CacheMixin();

	/**
	 * 
	 */
	public CachedStructureModule(boolean useCache) {
		cache.use(useCache);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public final void run(Request request, final Result result, final Callback callback) {
		final Object cacheKey = getCacheKey(request);
		LinkedList<EntityType> cached = cache.get().get(cacheKey, LinkedList.class);
		if (cache.use()) {
			produce(request, new DataCallback<List<EntityType>>() {

				@Override public void success(List<EntityType> entities) {
					cache.get().put(cacheKey, new LinkedList<EntityType>(entities));
					putResultAndReturn(result, entities, callback);
				}

				@Override public void failure() {
					callback.failure();
				}
			});
		} else {
			putResultAndReturn(result, cached, callback);
		}
	}

	private void putResultAndReturn(Result result, List<EntityType> entities, Callback callback) {
		result.getEntity().addAll(entities);
		callback.success();
	}

	protected abstract void produce(Request request, DataCallback<List<EntityType>> callback);

	protected abstract Object getCacheKey(Request request);
}
