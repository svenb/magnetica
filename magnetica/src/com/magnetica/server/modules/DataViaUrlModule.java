/**
 * 
 */
package com.magnetica.server.modules;

import gwt.tools.shared.Util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.data.Data;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.DataModule;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * @param <DataElementType>
 *            The type of DataElement this module is producing.
 * 
 */
public abstract class DataViaUrlModule<EntityType extends Entity, DataElementType extends DataElement> extends
	SimpleUrlRetrieverModule<EntityType> implements DataModule<EntityType, DataElementType> {

	protected final Set<Class<? extends DataElement>> dependsOn = new HashSet<Class<? extends DataElement>>();
	protected final HashSet<Class<Data>> suitable = new HashSet<Class<Data>>();
	private final ForwardEntityAs entitiesForwardedAs;

	protected DataViaUrlModule() {
		this(null);
	}

	protected DataViaUrlModule(Class<? extends DataElement>... dependingOnDataElements) {
		this(null, dependingOnDataElements);
	}

	protected DataViaUrlModule(String acceptsContentType, Class<? extends DataElement>... dependingOnDataElements) {
		this(acceptsContentType, MagneticaSettings.standardEntityForwarding(), Util.createList(dependingOnDataElements));
	}

	protected DataViaUrlModule(String acceptsContentType, ForwardEntityAs entitiesForwardedAs,
		List<Class<? extends DataElement>> dependingOnDataElements) {
		super(acceptsContentType);
		this.entitiesForwardedAs = (entitiesForwardedAs != null) ? entitiesForwardedAs : MagneticaSettings
			.standardEntityForwarding();
		//		dependsOn.addAll(dependingOnDataElements);
		Util.addAll(dependsOn, dependingOnDataElements);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.DataModule#dependsOnDataElements()
	 */
	@Override public Set<Class<? extends DataElement>> dependsOnDataElements() {
		return dependsOn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.DataModule#suitableEntityTypes()
	 */
	@Override public Set<Class<Data>> suitableEntityTypes() {
		return suitable;
	}

	@SuppressWarnings("unchecked") protected void addDataElementDependingOn(Class<? extends DataElement> dataElementType) {
		this.dependsOn.add(dataElementType);
	}

	protected void suitableForAnyEntityType() {
		assert suitable.size() == 0 : "Can not make data module suitable for all after adding a entity type; It's also not allowed to call suitableForAnyEntityType() more than once.";
		suitable.clear();
		suitable.add(Data.class);
	}

	@SuppressWarnings("unchecked") protected void addEntityTypeSuitableFor(Class<? extends Data> entityType) {
		suitable.add((Class<Data>) entityType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.SimpleUrlRetrieverModule#processResult(com
	 * .magnetica.shared.data.Request, com.magnetica.shared.data.Result, byte[])
	 */
	@Override protected boolean processResult(Request<EntityType> request, Result<EntityType> result, byte[] rawData) {
		DataElement data = processResult(rawData);
		setResult(request, result, data);
		return data != null;
	}

	protected void setResult(Request<EntityType> request, Result<EntityType> result, DataElement data) {
		getEntity(request, result).setData(data);
	}

	protected EntityType getEntity(Request<EntityType> request, Result<EntityType> result) {
		switch (entitiesForwardedAs) {
		case REQUEST:
			return request.getEntity();
		default:
			return result.getEntity();
		}
	}

	/**
	 * @param data
	 * @return
	 */
	protected abstract DataElement processResult(byte[] rawData);
}
