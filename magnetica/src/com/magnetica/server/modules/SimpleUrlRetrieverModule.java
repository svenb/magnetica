/**
 * 
 */
package com.magnetica.server.modules;

import gwt.tools.shared.Log;
import gwt.tools.shared.Util;

import java.io.IOException;
import java.net.MalformedURLException;

import com.google.apphosting.api.ApiProxy.ApiDeadlineExceededException;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;

/**
 * Use this simple module to receive data from a URL by implementing two
 * methods: getURL(request, result) which is supposed to generate a valid URL
 * and processResult(request, result, data) which will be called if data could
 * be loaded from the generated URL successfully.
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            Entity or a descendant.
 * 
 */
public abstract class SimpleUrlRetrieverModule<EntityType extends Entity> extends CachedUrlRetrieverModule<EntityType> {

	private final String acceptsContentType;

	/**
	 * Creates module with default configuration.
	 */
	public SimpleUrlRetrieverModule() {
		this(null);
	}

	/**
	 * Creates module which will request the given content from the URL.
	 * 
	 * @param acceptsContentType
	 */
	public SimpleUrlRetrieverModule(String acceptsContentType) {
		this.acceptsContentType = acceptsContentType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public void run(final Request<EntityType> request, final Result<EntityType> result,
		final Callback callback) {
		try {
			final String url = getUrl(request, result);
			// either no timeout or the remaining millis till the general timeout + 200ms buffer to make sure we can handle all the timeouts before the overall deadline 
			final long millisLeftUntilTimeout = request.hasTimeout() ? request.getTimeout()
				- System.currentTimeMillis() : 0;
			// leave handling of timeout to underlying module - e.g. if 100ms left but the thing is in the cache: alright then :)
			//			if (request.hasTimeout()) {
			//				long currentMillis = System.currentTimeMillis();
			//				// see if there is actually enough time left
			//				millisLeftUntilTimeout = request.getTimeout() - currentMillis;
			//				Log.debug(this, ".run: req.timeout=" + request.getTimeout() + "; calcTimeout=" + millisLeftUntilTimeout
			//					+ "; url=" + url);
			//				if (millisLeftUntilTimeout < 200) {
			//					// not worth trying
			//					Log.warn(SimpleUrlRetrieverModule.class, ".run: not tried because time left is too short. url="
			//						+ url);
			//					callback.failure();
			//					return;
			//				}
			//			} else {
			//				millisLeftUntilTimeout = 0;
			//			}
			loadUrl(url, millisLeftUntilTimeout, acceptsContentType, new AsyncCallback<byte[]>() {

				@Override public void onSuccess(byte[] data) {
					Log.debug(this, ".run.succeeded: #bytes=" + Util.size(data) + "; url=" + url);
					if (processResult(request, result, data)) {
						callback.success();
					} else {
						callback.failure();
					}
				}

				@Override public void onFailure(Throwable caught) {
					if (caught instanceof IOException || caught instanceof ApiDeadlineExceededException
						|| caught instanceof java.net.SocketTimeoutException) {
						Log.warn(SimpleUrlRetrieverModule.class, ".run: failed due to timeout ("
							+ millisLeftUntilTimeout + "ms). message=" + caught.getMessage() + "; url=" + url);
						callback.failure(); // IOException might be timeout or network error; other are timeouts, retry next time
					} else {
						Log.warn(SimpleUrlRetrieverModule.class, ".run: failed. url=" + url, caught);
						callback.success(); // permanent error, thus, there is no data to get so it's fine.
					}

				}
			});
		} catch (MalformedURLException e) {
			Log.warn(this, "The provided URL is invalid!", e);
			callback.success(); // nothing to retry here
		}
	}

	/**
	 * @param result
	 * @param data
	 * @return False if the request should be retried the next time.
	 */
	protected abstract boolean processResult(Request<EntityType> request, Result<EntityType> result, byte[] data);

	/**
	 * @param request
	 * @return
	 */
	protected abstract String getUrl(Request<EntityType> request, Result<EntityType> result);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.generic.BasicModule#testModule(com.magnetica
	 * .shared.SimpleCallback)
	 */
	@Override protected void testModule(final SimpleCallback callback) {
		String url = getTestUrl();
		if (Util.isEmpty(url)) {
			// test not implemented
			Log.debug(this, "test not implemented.");
			callback.success();
			return;
		}
		try {
			loadUrl(url, new AsyncCallback<byte[]>() {

				@Override public void onSuccess(byte[] result) {
					if (validateTestResult(result)) {
						callback.success();
					} else {
						callback.failure();
					}
				}

				@Override public void onFailure(Throwable caught) {
					callback.failure();
				}
			});
		} catch (MalformedURLException e) {
			Log.severe(this, "test: invalid URL=" + url, e);
		}
	}

	protected String getTestUrl() {
		return null;
	}

	protected boolean validateTestResult(byte[] rawData) {
		return Util.notEmpty(rawData);
	}
}
