/**
 * 
 */
package com.magnetica.server.modules;

import java.util.List;

import com.magnetica.shared.CacheMixin;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * @deprecated Use @DataViaUrlModule directly as it has caching built in
 *             already. Or a cached rack if loading multiple things in parallel.
 * @author Sven Buschbeck
 * @param <EntityType>
 * @param <DataType>
 * 
 */
@Deprecated public abstract class CachedDataViaUrlModule<EntityType extends Entity, DataType extends DataElement>
	extends DataViaUrlModule<EntityType, DataType> {

	private static final String NAME = CachedDataViaUrlModule.class.getName();
	protected CacheMixin cache = new CacheMixin();
	private final String PROVIDED_DATA_ELEMENT_NAME;

	protected CachedDataViaUrlModule(Boolean useCache, String acceptsContentType, ForwardEntityAs entitiesForwardedAs,
		List<Class<? extends DataElement>> dependingOnDataElements) {
		super(acceptsContentType, entitiesForwardedAs, dependingOnDataElements);
		Class<DataType> providesDataElement = providesDataElement();
		PROVIDED_DATA_ELEMENT_NAME = (providesDataElement != null) ? providesDataElement.getClass().getName() : "";
		if (useCache != null) {
			cache.use(useCache);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public final void run(Request<EntityType> request, final Result<EntityType> result,
		final Callback callback) {
		if (!cache.use()) {
			super.run(request, result, callback);
			return;
		}

		final Object cacheKey = getCacheKey(request, result);
		DataType cached = cache.get().get(cacheKey, providesDataElement());
		if (cached == null) {
			//			produce(request, new DataCallback<DataType>() {
			//
			//				@Override public void success(DataType dataElement) {
			//					cache.get().put(cacheKey, dataElement);
			//					putResultAndReturn(result, dataElement, callback);
			//				}
			//
			//				@Override public void failure() {
			//					callback.failure();
			//				}
			//			});
			super.run(request, result, callback);
		} else {
			//			putResultAndReturn(result, cached, callback);
			super.setResult(request, result, cached);
			callback.success();
		}
	}

	//	private void putResultAndReturn(Result<EntityType> result, DataType data, Callback callback) {
	//		result.getEntity().setData(data);
	//		callback.success();
	//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.DataViaUrlModule#setResult(com.magnetica
	 * .shared.data.Request, com.magnetica.shared.data.Result,
	 * com.magnetica.shared.data.DataElement)
	 */
	@Override protected void setResult(Request<EntityType> request, Result<EntityType> result, DataElement dataElement) {
		// cache produced result
		cache.get().put(getCacheKey(request, result), dataElement);
		super.setResult(request, result, dataElement);
	}

	//	protected abstract void produce(Request<EntityType> request, DataCallback<DataType> callback);

	/**
	 * Gets the key to be used for caching. defaults to the <module class name +
	 * class name of the provided data element + generated URL>. Overwrite to
	 * create custom cache keys
	 */
	protected Object getCacheKey(Request<EntityType> request, Result<EntityType> result) {
		return NAME + PROVIDED_DATA_ELEMENT_NAME + getUrl(request, result);
	}

}
