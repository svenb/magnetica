/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface MetaData extends Serializable {

}
