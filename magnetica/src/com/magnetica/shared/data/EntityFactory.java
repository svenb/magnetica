/**
 * 
 */
package com.magnetica.shared.data;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface EntityFactory {

	Entity createEntity(String id);

}
