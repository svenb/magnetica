/**
 * 
 */
package com.magnetica.shared.data;

import gwt.tools.shared.Util;
import gwt.tools.shared.dto.ExtendedToString;
import gwt.tools.shared.dto.HasId;
import gwt.tools.shared.dto.HasLabel;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Entity extends LinkedList<Entity> implements Serializable, HasId, HasLabel, ExtendedToString {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6762125228664895883L;
	private Data data;

	/**
	 * 
	 */
	public Entity() {
	}

	public Entity(DataElement... dataElements) {
		for (DataElement dataElement : dataElements) {
			setData(dataElement);
		}
	}

	public Entity(DataElement dataElement) {
		setData(dataElement);
	}

	public Entity(Collection<? extends DataElement> dataElements, Entity... childEntities) {
		this(childEntities);
		for (DataElement dataElement : dataElements) {
			setData(dataElement);
		}
	}

	public Entity(Entity... childEntities) {
		for (Entity childEntity : childEntities) {
			add(childEntity);
		}

	}

	public Entity(DataElement dataElement, Entity... childEntities) {
		this(childEntities);
		setData(dataElement);
	}

	public void adaptTo(Entity entity) {
		getData().adaptTo(entity.getData());
		clear();
		addAll(entity);
	}

	// Related entities shortcut //////////////////////////////////////////////

	/**
	 * @param attribute
	 * @return
	 */
	public boolean contains(Entity entity) {
		return super.contains(entity);
	}

	public LinkedList<Entity> getLeafs() {
		LinkedList<Entity> results = new LinkedList<Entity>();
		if (Util.isEmpty(this)) {
			results.add(this);
		} else {
			for (Entity entity : this) {
				results.addAll(entity.getLeafs());
			}
		}
		return results;
	}

	public LinkedList<LinkedList<Entity>> getLevels() {
		LinkedList<LinkedList<Entity>> levels = new LinkedList<LinkedList<Entity>>();
		LinkedList<Entity> me = new LinkedList<Entity>();
		me.add(this);
		levels.add(me);
		addSubLevels(1, levels);
		return levels;
	}

	private void addSubLevels(int currentLevel, LinkedList<LinkedList<Entity>> levels) {
		if (levels.size() <= currentLevel) {
			levels.add(new LinkedList<Entity>());
		}
		LinkedList<Entity> level = levels.get(currentLevel);
		level.addAll(this);
		for (Entity entity : this) {
			entity.addSubLevels(currentLevel + 1, levels);
		}
	}

	public LinkedList<Entity> getAll(boolean recursive, boolean includeMe) {
		return getAll((Callback) null, recursive, includeMe);
	}

	public LinkedList<Entity> getAll(final Class<? extends DataElement> thatHaveThisDataElement, boolean recursive,
		boolean includeMe) {
		return getAll(new Callback() {

			@Override public boolean onCallback(Entity entity) {
				return entity.hasData(thatHaveThisDataElement);
			}
		}, recursive, includeMe);
	}

	public LinkedList<Entity> getAll(Callback entityShouldBeIncludedCallback, boolean recursive, boolean includeMe) {
		if (recursive) {
			return getAllRecursively(entityShouldBeIncludedCallback, includeMe);
		}
		if (entityShouldBeIncludedCallback == null) {
			LinkedList<Entity> all = new LinkedList<Entity>(this);
			if (includeMe) {
				all.addFirst(this);
			}
			return all;
		}
		LinkedList<Entity> all = new LinkedList<Entity>();
		if (includeMe && entityShouldBeIncludedCallback.onCallback(this)) {
			all.add(this);
		}
		for (Entity subEntity : this) {
			if (entityShouldBeIncludedCallback.onCallback(subEntity)) {
				all.add(subEntity);
			}
		}
		return all;
	}

	private LinkedList<Entity> getAllRecursively(Callback entityShouldBeIncludedCallback, boolean includeMe) {
		LinkedList<Entity> results = new LinkedList<Entity>();
		if (includeMe && (entityShouldBeIncludedCallback == null || entityShouldBeIncludedCallback.onCallback(this))) {
			results.add(this);
		}
		for (Entity entity : this) {
			results.addAll(entity.getAllRecursively(entityShouldBeIncludedCallback, true));
		}
		return results;
	}

	public static interface Callback {
		boolean onCallback(Entity entity);
	}

	//	public Collection<Entity> getEntitiesRecursively(final Class<? extends DataElement> thatHaveThisDataElement) {
	//		return getEntitiesRecursively(new Callback() {
	//
	//			@Override public boolean onCallback(Entity entity) {
	//				return entity.hasData(thatHaveThisDataElement);
	//			}
	//		});
	//	}
	//
	//	public Collection<Entity> getEntitiesRecursively(Callback entityShouldBeIncludedCallback) {
	//		assert entityShouldBeIncludedCallback != null;
	//		Collection<Entity> results = new LinkedList<Entity>();
	//		for (Entity entity : getAllRecursively(true)) {
	//			if (entityShouldBeIncludedCallback.onCallback(entity)) {
	//				results.add(entity);
	//			}
	//		}
	//		return results;
	//	}

	/**
	 * Returns the first entity which has a DataElement of the given type and
	 * that DataElement's ID equals the given ID.
	 * 
	 * @param dataType
	 * @param id
	 * @param recursive
	 * @param includeMe
	 * @return first entity where entity.getData(DataType).getId().equals(id)
	 */
	public <DataType extends DataElement & HasId> Entity get(final Class<DataType> dataType, String id,
		boolean recursive, boolean includeMe) {
		// OPTIMIZE instead of getting all first, check each entity while walking through the tree if it fits. With that we have to walk the whole tree only in the worst case; Though, running through all of them makes sure there is only one, thus, this version could be used for debugging, e.g. by adding a flag "ensureOnlyOne"
		LinkedList<Entity> all = getAll((dataType != null) ? new Callback() {

			@Override public boolean onCallback(Entity entity) {
				return entity.hasData(dataType);
			}
		} : null, recursive, includeMe);
		LinkedList<Entity> result = new LinkedList<Entity>();
		for (Entity entity : all) {
			if (Util.equals(id, entity.getId(dataType))) {
				result.add(entity);
			}
		}
		assert result.size() < 2 : result;
		return Util.isEmpty(result) ? null : result.getFirst();
	}

	public Entity get(String id, boolean recursive) {
		return get(id, recursive, true);
	}

	public Entity get(String id, boolean recursive, boolean includeMe) {
		return Util.getById(recursive ? getAll(recursive, includeMe) : this, id);
	}

	/**
	 * Extracts all the entities defined by the filter callback but then also
	 * add the ones between the root and the filtered ones so that the result is
	 * a fully traversable partial tree
	 * 
	 * @param entityShouldBeIncludedCallback
	 * @return
	 */
	public Entity getPartialTree(Callback entityShouldBeIncludedCallback) {
		// get all the entities that should be included
		//		Collection<Entity> toBeIncluded = getEntitiesRecursively(entityShouldBeIncludedCallback);
		Collection<Entity> toBeIncluded = getAll(entityShouldBeIncludedCallback, true, true);
		// add all nodes that link the required entities with this entity
		for (Entity include : new LinkedList<Entity>(toBeIncluded)) {
			for (Entity pathEntity : getPathTo(include, false)) {
				if (!toBeIncluded.contains(pathEntity)) {
					toBeIncluded.add(pathEntity);
				}
			}
		}
		// build up the tree
		return clone(toBeIncluded);
	}

	/**
	 * Extracts all the entities by the given IDs but then also add the ones
	 * between the root and the filtered ones so that the result is a fully
	 * traversable partial tree
	 * 
	 * @param ids
	 * @return
	 */
	public Entity getPartialTree(final Collection<String> ids) {
		return getPartialTree(new Callback() {

			@Override public boolean onCallback(Entity entity) {
				return ids.contains(entity.getId());
			}
		});
	}

	/**
	 * Adds all sub-entities of entity. Optionally, replaces existing children
	 * if equal.
	 * 
	 * @param entity
	 * @param replaceChildrenIfContained
	 * @return this for chaining
	 */
	public Entity addAllChildEntities(Entity entity, boolean replaceChildrenIfContained) {
		if (!replaceChildrenIfContained) {
			addAll(entity);
		} else {
			for (Entity relatedEntity : entity) {
				if (contains(relatedEntity)) {
					if (replaceChildrenIfContained) {
						int index = indexOf(relatedEntity);
						remove(index);
						add(index, relatedEntity);
					}
				}
			}
		}
		return this;
	}

	public Entity clone(Collection<Entity> useOnlyThisEntities) {
		Entity clone = clone(false, false, false);
		for (Entity sub : this) {
			if (useOnlyThisEntities.contains(sub)) {
				clone.add(sub.clone(useOnlyThisEntities));
			}
		}
		return clone;
	}

	public Entity clone(boolean recursively, boolean cloneData, boolean cloneDataElements) {
		Entity clone = new Entity();
		clone.data = cloneData ? this.data.clone(cloneDataElements) : this.data;
		if (recursively) {
			for (Entity children : this) {
				clone.add(children.clone(recursively, cloneData, cloneDataElements));
			}
		}
		return clone;
	}

	// Data short cuts ////////////////////////////////////////////////////////

	/**
	 * Returns the first ID found in all the DataElements available.
	 * 
	 * @return ID of first DataElement implementing HasId
	 */
	@Override public String getId() {
		assert getData() != null : "This entity does not have any data available to extract an ID from. this=" + this;
		return (getData() != null) ? getData().getId() : null;
	}

	/**
	 * Gets the ID of the given DataElement (which must implement HasId). As
	 * opposed to {@link #getId()} which will always return the first ID found
	 * in all the DataElements available.
	 * 
	 * @param dataType
	 * @return ID of DataElement of given DataType.
	 */
	public <DataType extends DataElement & HasId> String getId(Class<DataType> dataType) {
		assert hasData(dataType) : "This entity does not have any data available to extract an ID from. this=" + this;
		return (hasData(dataType)) ? getData(dataType).getId() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.dto.HasLabel#getLabel()
	 */
	@Override public String getLabel() {
		assert getData() != null : "This entity does not have any data available to extract a label from. this=" + this;
		return (getData() != null) ? getData().getLabel() : null;
	}

	/**
	 * @return
	 */
	public Class<? extends Data> getDataType() {
		return (getData() == null) ? null : getData().getClass();
	}

	public boolean hasData() {
		return data != null && !data.isEmpty();
	}

	public <DataType extends DataElement> boolean hasData(Class<DataType> dataType) {
		return getData(true).hasData(dataType);
	}

	public Data getData() {
		return getData(true);
	}

	public Data getData(boolean createOnDemand) {
		return (data == null && createOnDemand) ? data = new Data() : data;
	}

	public <DataType extends DataElement> DataType getData(Class<DataType> dataType) {
		return getData(true).get(dataType);
	}

	public void setData(Data data) {
		this.data = data;
	}

	public void setData(DataElement data) {
		// TODO is it OK to filter null out? Of course, null means no class so this wont work anyway but maybe null is the desired information that will get lost here.
		if (data != null) {
			getData(true).put(data);
		}
	}

	public Set<Class<? extends DataElement>> getDataElementTypes() {
		Set<Class<? extends DataElement>> types = new HashSet<Class<? extends DataElement>>();
		if (hasData()) {
			for (DataElement element : getData()) {
				types.add(element.getClass());
			}
		}
		return types;
	}

	/**
	 * Returns true if this entity as *neither* data attached to it nor sub
	 * entities.
	 */
	@Override public boolean isEmpty() {
		return !hasData() && Util.isEmpty(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractList#hashCode()
	 */
	@Override public int hashCode() {
		return getId().hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractList#equals(java.lang.Object)
	 */
	@Override public boolean equals(Object o) {
		return this == o || (o instanceof Entity && equals((Entity) o));
	}

	public boolean equals(Entity otherEntity) {
		//		try {
		//		return otherEntity != null && getId().equals(otherEntity.getId());
		return Util.equals(this, otherEntity);
		//		} catch (Throwable t) { // assertion or runtime error if no ID is provided by the data - we just say not equal in this case
		//			return false;
		//		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override public String toString() {
		return "Entity(#data=" + getData().size() + "; #subentities=" + size() + "; " + data
		// + ", " + super.toString()
			+ ")";
	}

	/**
	 * Relation of number of entity by expected number of entities.
	 * 
	 * @param expectedSubNodes
	 * @return
	 */
	public float getStructureCompleteness(int expectedSubNodes) {
		return ((float) size()) / expectedSubNodes;
	}

	/**
	 * Value of 0..1; 1 means all entities have all required data elements, 0:
	 * none has;
	 * 
	 * @param requiredDataElements
	 * @return 0..1 relating to 0% to 100%
	 */
	public float getDataCompleteness(Class<DataElement>... requiredDataElements) {
		int dataElementCount = 0;
		for (Entity entity : this) {
			for (Class<DataElement> dataElementClass : requiredDataElements) {
				if (entity.hasData(dataElementClass)) {
					dataElementCount++;
				}
			}
		}
		int required = size() * requiredDataElements.length;
		return required == 0 ? 1 : ((float) dataElementCount / required);
	}

	/**
	 * @param b
	 * @return
	 */
	public String toString(boolean extended) {
		if (!extended) {
			return toString();
		}
		StringBuilder b = new StringBuilder();
		toString(b, 0);
		return b.toString();
	}

	public void toString(StringBuilder b, int level) {
		b.append(Util.getIndentString(level));
		b.append(toString());
		if (size() > 0) {
			b.append("\n");
			for (Entity subEntity : this) {
				subEntity.toString(b, level + 1);
			}
		}
	}

	public Entity getParent(Entity entityTreeRoot) {
		assert entityTreeRoot != null;
		if (this == entityTreeRoot) {
			return null;
		}
		LinkedList<Entity> path = entityTreeRoot.getPathTo(this, true);
		if (Util.notEmpty(path)) {
			assert path.getLast() == this : "path(#" + path.size() + "=" + path;
			assert path.size() > 1;
			return path.get(path.size() - 2);
		}
		return null;
	}

	/**
	 * Returns a list of all entities between the given root entity and this
	 * entity - including the root entity.
	 * 
	 * @param rootEntity
	 * @return list of all parents of this entity.
	 */
	public LinkedList<Entity> getParents(Entity rootEntity) {
		LinkedList<Entity> path = rootEntity.getPathTo(this, true);
		if (path != null && path.getLast() == this) {
			path.removeLast();
		}
		return path;
	}

	/**
	 * @param entity
	 */
	public LinkedList<Entity> getPathTo(Entity entity, boolean includeMe) {
		LinkedList<Entity> path = new LinkedList<Entity>();
		if (getPathTo(entity, path)) {
			if (includeMe) {
				path.addFirst(this);
			}
			return path;
		}
		return null;
	}

	private boolean getPathTo(Entity entity, LinkedList<Entity> path) {
		if (entity == this) {
			return true;
		}
		for (Entity subEntity : this) {
			if (subEntity.getPathTo(entity, path)) {
				path.addFirst(subEntity);
				return true;
			}
		}
		return false;
	}

	/**
	 * Calculates the maximum depth of the tree of entities starting from this
	 * node.
	 * 
	 * @return Number of levels below.
	 */
	public int getMaxDepth() {
		return getMaxDepth(0);
	}

	public int getMaxDepth(int currentDepth) {
		int maxDepth = currentDepth;
		currentDepth++;
		for (Entity child : this) {
			maxDepth = Math.max(maxDepth, child.getMaxDepth(currentDepth));
		}
		return maxDepth;
	}
}
