/**
 * 
 */
package com.magnetica.shared.data;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Cacheable {

	String getCacheKey();

}
