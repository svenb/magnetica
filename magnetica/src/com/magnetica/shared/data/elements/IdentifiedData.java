/**
 * 
 */
package com.magnetica.shared.data.elements;

import gwt.tools.shared.dto.HasId;

import com.magnetica.shared.data.DataElement;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class IdentifiedData implements DataElement, HasId {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
