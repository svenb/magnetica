/**
 * 
 */
package com.magnetica.shared.data.elements;

import com.magnetica.shared.data.DataElement;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class StringData implements DataElement {

	public String data;

}
