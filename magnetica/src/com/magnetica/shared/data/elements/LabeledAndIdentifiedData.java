/**
 * 
 */
package com.magnetica.shared.data.elements;

import gwt.tools.shared.dto.HasLabel;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class LabeledAndIdentifiedData extends IdentifiedData implements HasLabel {

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
