/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface DataElement extends Serializable { //<DataType extends DataElement> {

	//	Class<DataType> getType();

}
