/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class WithMetadata implements Serializable, Cacheable {

	private MetaData metadata;

	/**
	 * 
	 */
	@Deprecated public WithMetadata() {
	}

	public WithMetadata(MetaData metadata) {
		this.setMetadata(metadata);
	}

	public MetaData getMetadata() {
		return metadata;
	}

	public void setMetadata(MetaData metadata) {
		this.metadata = metadata;
	}

	//	public void setMetadata(MetaData metadata) {
	//		this.metadata = metadata;
	//	}

	public void adopt(WithMetadata withMetadata) {
		this.setMetadata((withMetadata != null) ? withMetadata.getMetadata() : null);
	}

	public boolean isCacheable() {
		return metadata instanceof Cacheable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.data.Cacheable#getCacheKey()
	 */
	@Override public String getCacheKey() {
		assert metadata instanceof Cacheable;
		return ((Cacheable) metadata).getCacheKey();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return "WithMetadata(" + getMetadata() + ")";
	}
}
