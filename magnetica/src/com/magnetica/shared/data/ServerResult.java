/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public class ServerResult<EntityType extends Entity> implements Serializable {

	private boolean successful;
	private Result<EntityType> result;

	/**
	 * 
	 */
	@Deprecated public ServerResult() {
	}

	public ServerResult(Result<EntityType> result, boolean successful) {
		this.result = result;
		this.successful = successful;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public Result<EntityType> getResult() {
		return result;
	}

}
