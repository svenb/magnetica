/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;
import java.util.LinkedList;

public class Request<EntityType extends Entity> extends WithMetadata implements Serializable, Cacheable {
	/**
	 * 
	 */
	private static final int DEFAULT_TIMEOUT = 0;
	private EntityType entity;
	long timeout = 0;
	boolean cancelled = false;
	@Deprecated int depth;
	private LinkedList<Request<EntityType>> subRequests = new LinkedList<Request<EntityType>>();

	/**
	 * 
	 */
	@Deprecated public Request() {
		this(null, DEFAULT_TIMEOUT);
	}

	/**
	 * @param entity
	 */
	public Request(EntityType entity, long timeout) {
		this(entity, timeout, null);
	}

	public Request(EntityType entity, MetaData metadata) {
		this(entity, DEFAULT_TIMEOUT, metadata);
	}

	public Request(EntityType entity, long timeout, MetaData metadata) {
		this(entity, timeout, -1, metadata);
	}

	public Request(EntityType entity, long timeout, int depth, MetaData metadata) {
		super(metadata);
		this.entity = entity;
		setTimeout(timeout);
		this.depth = depth;
	}

	public void addSubRequest(Request<EntityType> request) {
		subRequests.add(request);
	}

	/**
	 * @return
	 */
	public long getTimeout() {
		return timeout;
	}

	public boolean hasTimeout() {
		return getTimeout() != 0;
	}

	public EntityType getEntity() {
		return entity;
	}

	public void setEntity(EntityType entity) {
		this.entity = entity;
	}

	/**
	 * @param i
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.data.WithMetadata#toString()
	 */
	@Override public String toString() {
		return "Request(" + entity + ", " + timeout + ", " + depth + ", " + super.toString() + ")";
	}

	public String getCacheKey() {
		// OPTIMIZE cache cacheKey
		return "REQ" + depth + ";" + super.getCacheKey()
			+ ((entity != null && !entity.isEmpty()) ? ";" + entity.toString(true) : "");
	}

	public void cancel() {
		cancelled = true;
		for (Request<EntityType> subRequest : subRequests) {
			subRequest.cancel();
		}
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public Request<EntityType> clone(EntityType newEntityToBeUsed, boolean cloneSubRequests) {
		Request<EntityType> clone = new Request<EntityType>(newEntityToBeUsed, timeout, depth, getMetadata());
		clone.cancelled = cancelled;
		if (cloneSubRequests) {
			clone.subRequests = subRequests;
		}
		return clone;
	}

	public Request<EntityType> cloneAsSubRequest(EntityType newEntityToBeUsed) {
		Request<EntityType> clone = clone(newEntityToBeUsed, false);
		addSubRequest(clone);
		return clone;
	}
}