/**
 * 
 */
package com.magnetica.shared.data;

import gwt.tools.client.Util;
import gwt.tools.shared.dto.HasId;
import gwt.tools.shared.dto.HasLabel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class Data implements Serializable, Iterable<DataElement>, HasId, HasLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -333593638528987114L;

	private HashMap<String, DataElement> dataElements;

	private HasId idGivingDataElement = null;
	private HasLabel labelGivingDataElement = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.dto.HasId#getId()
	 */
	@Override public String getId() {
		if (idGivingDataElement == null) {
			for (DataElement element : getMap().values()) {
				if (element instanceof HasId) {
					idGivingDataElement = (HasId) element;
					break;
				}
			}
		}
		assert idGivingDataElement != null : "There is no DataElement that implements HasId so that magnetica could not find an ID for this entity. "
			+ this;
		return (idGivingDataElement != null) ? idGivingDataElement.getId() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwt.tools.shared.dto.HasLabel#getLabel()
	 */
	@Override public String getLabel() {
		if (labelGivingDataElement == null) {
			for (DataElement element : getMap().values()) {
				if (element instanceof HasLabel) {
					labelGivingDataElement = (HasLabel) element;
					break;
				}
			}
		}
		assert labelGivingDataElement != null : "There is no DataElement that implements HasLabel so that magnetica could not find a label for this entity. "
			+ this;
		return (labelGivingDataElement != null) ? labelGivingDataElement.getLabel() : null;
	}

	public <DataElementClass extends DataElement> DataElementClass get(Class<DataElementClass> dataType) {
		DataElement dataElement = getMap().get(dataType.getName());
		if (dataElement == null) {
			for (DataElement aDataElement : getMap().values()) {
				// Class.isAssignableFrom is not support in GWT client-side :(
				//if (dataType.isAssignableFrom(dataElement.getClass())) {
				if (Util.isObjectAssignableTo(aDataElement, dataType)) {
					dataElement = aDataElement;
					break;
				}
			}
		}
		return (DataElementClass) dataElement;
	}

	public DataElement put(DataElement value) {
		return put((Class<DataElement>) value.getClass(), value);
	}

	public <DataElementType extends DataElement> DataElement put(Class<DataElementType> key, DataElementType value) {
		idGivingDataElement = null;
		return getMap().put(key.getName(), value);
	}

	public <DataType extends DataElement> boolean hasData(Class<DataType> dataType) {
		// FIXME hasData can require the same effort as getting the data element; check if compiler handles that; if not: remove hasData to force usage of getData(...) and checking the result against null.
		if (getMap().containsKey(dataType.getName())) {
			return true; // short cut
		}
		return get(dataType) != null;
	}

	private HashMap<String, DataElement> getMap() {
		return (dataElements == null) ? dataElements = new HashMap<String, DataElement>() : dataElements;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override public Iterator<DataElement> iterator() {
		return getMap().values().iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return "Data(" + dataElements + ")";
	}

	/**
	 * @return
	 */
	public int size() {
		return getMap().size();
	}

	public boolean isEmpty() {
		return getMap().isEmpty();
	}

	/**
	 * @param data
	 */
	public void adaptTo(Data data) {
		HashMap<String, DataElement> map = getMap();
		map.clear();
		map.putAll(data.getMap());
	}

	protected Data clone(boolean cloneDataElements) {
		Data clone = new Data();
		clone.dataElements = this.dataElements;
		if (cloneDataElements) {
			throw new RuntimeException("Not Implemented");
		}
		return clone;
	}
}
