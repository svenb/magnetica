/**
 * 
 */
package com.magnetica.shared.data;

import java.io.Serializable;

public class Result<EntityType extends Entity> extends WithMetadata implements Serializable {
	private EntityType entity;

	/**
	 * 
	 */
	@Deprecated public Result() {
	}

	public Result(EntityType entity) {
		this(entity, null);
	}

	public Result(EntityType entity, MetaData metaData) {
		super(metaData);
		this.entity = entity;
	}

	/**
	 * @return
	 */
	public EntityType getEntity() {
		return entity;
	}

	/**
	 * @param entity2
	 */
	public void setEntity(EntityType entity) {
		this.entity = entity;
	}

	public void adopt(Result<EntityType> result) {
		super.adopt(result);
		this.entity = (result != null) ? result.entity : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return "Result(" + entity + ", " + super.toString() + ")";
	}

}