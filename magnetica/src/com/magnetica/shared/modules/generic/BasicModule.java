/**
 * 
 */
package com.magnetica.shared.modules.generic;

import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public abstract class BasicModule<EntityType extends Entity> implements Module<EntityType> {

	private static final long CHECK_WORKING_INTERVAL = 5 * 3600 * 1000; // 5 minutes
	private static final long CHECK_BROKEN_INTERVAL = 10000; // 10 seconds

	private boolean isWorking = true;
	private boolean testRunning = false;
	private long lastCheck = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.Module#isWorking()
	 */
	@Override public final synchronized boolean isWorking() {
		long currentTimeMillis = System.currentTimeMillis();
		if (!MagneticaSettings.testModules()) {
			return true;
		}
		if (!testRunning && (isWorking && (lastCheck + CHECK_WORKING_INTERVAL < currentTimeMillis))
			|| (!isWorking && (lastCheck + CHECK_BROKEN_INTERVAL < currentTimeMillis))) {
			testRunning = true;
			testModule(new SimpleCallback() {

				@Override public void done(boolean success) {
					isWorking = success;
					lastCheck = System.currentTimeMillis();
					testRunning = false;
				}
			});
		}
		return isWorking;
	}

	/**
	 * Checks request.getTimeout() againg System.currentMillis()
	 * 
	 * @param request
	 * @return timeout<system.time
	 */
	protected boolean timeIsUp(Request<?> request) {
		return request.getTimeout() > 0 && request.getTimeout() < System.currentTimeMillis();
	}

	/**
	 * Test if this module is working *now*
	 * 
	 */
	abstract protected void testModule(SimpleCallback callback);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#cancel(com.magnetica.shared.data.
	 * Request, com.magnetica.shared.data.Result)
	 */
	@Override public void cancel(Request<EntityType> request, Result<EntityType> result) {
		request.cancel();
	}
}
