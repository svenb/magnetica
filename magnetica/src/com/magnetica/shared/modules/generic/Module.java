/**
 * 
 */
package com.magnetica.shared.modules.generic;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Module<EntityType extends Entity> {

	class KeyValuePair {
		String key;
		Object value;
	}

	//	LoadLevel loadSync(Request request, Entity result) throws ServiceNotAvailableException;

	// OR
	void run(Request<EntityType> request, Result<EntityType> result, Callback callback);

	boolean isWorking();

	void cancel(Request<EntityType> request, Result<EntityType> result);

}
