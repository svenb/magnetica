/**
 * 
 */
package com.magnetica.shared.modules.generic;

import gwt.tools.shared.Util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.magnetica.shared.data.Data;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * @param <DataElementType>
 * 
 */
public abstract class BasicDataModule<EntityType extends Entity, DataElementType extends DataElement> extends
	BasicModule<EntityType> implements DataModule<EntityType, DataElementType> {

	private Set<Class<? extends DataElement>> dependsOn;
	private Set<Class<Data>> suitableTypes;

	protected BasicDataModule() {
		super();
	}

	protected BasicDataModule(Class<? extends DataElement>... dependingOnDataElements) {
		this(Util.createList(dependingOnDataElements));
	}

	protected BasicDataModule(List<Class<? extends DataElement>> list) {
		super();
		dependsOn = new HashSet<Class<? extends DataElement>>(list);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.generic.DataModule#dependsOnDataElements()
	 */
	@Override public Set<Class<? extends DataElement>> dependsOnDataElements() {
		return dependsOn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.generic.DataModule#suitableEntityTypes()
	 */
	@Override public Set<Class<Data>> suitableEntityTypes() {
		return suitableTypes;
	}

	/**
	 * 
	 */
	protected void suitableForAnyEntityType() {
		suitableTypes = null;
	}

	public boolean isSuitableForAnyEntityType() {
		return suitableTypes == null;
	}

}
