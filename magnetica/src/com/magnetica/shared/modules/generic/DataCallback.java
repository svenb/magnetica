/**
 * 
 */
package com.magnetica.shared.modules.generic;


public interface DataCallback<DataType> {//<DataType extends DataElement> {
	// TODO find simple and complex examples of requests, define modules for this example and see if the module definition is OK for that
	// TODO add LoadLevel to result
	// IDEA add info about currently possible max LoadLevel to result
	void success(DataType dataElement);

	void failure();
}