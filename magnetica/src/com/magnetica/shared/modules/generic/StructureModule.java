/**
 * 
 */
package com.magnetica.shared.modules.generic;

import com.magnetica.shared.data.Entity;

/**
 * A module that produces entities. In general, this kind of modules are invoked
 * before @DataModule's to build up an entity graph.
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public interface StructureModule<EntityType extends Entity> extends Module<EntityType> {

}
