/**
 * 
 */
package com.magnetica.shared.modules.generic;

import java.util.Set;

import com.magnetica.shared.data.Data;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface DataModule<EntityType extends Entity, DataType extends DataElement> extends Module<EntityType> {

	Set<Class<Data>> suitableEntityTypes();

	Set<Class<? extends DataElement>> dependsOnDataElements();

	Class<DataType> providesDataElement();

}
