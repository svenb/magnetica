/**
 * 
 */
package com.magnetica.shared.modules.generic;

import com.magnetica.shared.data.Entity;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public abstract class BasicStructureModule<EntityType extends Entity> extends BasicModule<EntityType> implements
	StructureModule<EntityType> {

}
