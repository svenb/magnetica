/**
 * 
 */
package com.magnetica.shared.modules;

import java.util.HashMap;
import java.util.Map;

import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.MetaData;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.BasicModule;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class DispatcherModule<EntityType extends Entity> extends BasicModule<EntityType> {

	/**
	 * 
	 */
	public static final boolean STOP_ON_FIRST_MATCH_DEFAULT = false;
	public final Map<Class<? extends MetaData>, Module<EntityType>> mappings;
	public final boolean stopOnFirstMatch;

	public DispatcherModule() {
		this(STOP_ON_FIRST_MATCH_DEFAULT);
	}

	public DispatcherModule(boolean stopOnFirstMatch) {
		this(new HashMap<Class<? extends MetaData>, Module<EntityType>>());
	}

	public DispatcherModule(Map<Class<? extends MetaData>, Module<EntityType>> metaDataToModuleMap) {
		this(metaDataToModuleMap, false);
	}

	public DispatcherModule(Map<Class<? extends MetaData>, Module<EntityType>> metaDataToModuleMap,
		boolean stopOnFirstMatch) {
		this.mappings = metaDataToModuleMap;
		this.stopOnFirstMatch = stopOnFirstMatch;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public void run(Request<EntityType> request, Result<EntityType> result, Callback callback) {
		Class<? extends MetaData> metadataClass = request.getMetadata().getClass();
		for (Class<? extends MetaData> mappingMetadataClass : mappings.keySet()) {
			//			if (mappingMetadataClass.isAssignableFrom(metadataClass)) {
			// "isAssignableFrom()" not available on client side
			if (mappingMetadataClass.getName().equals(metadataClass.getName())) {
				mappings.get(mappingMetadataClass).run(request, result, callback);
				if (stopOnFirstMatch) {
					break;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModule#testModule(com.magnetica.shared
	 * .SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		callback.done(mappings.size() > 0);
	}

}
