/**
 * 
 */
package com.magnetica.shared;

import gwt.tools.shared.CommonCache;

import com.google.gwt.core.shared.GWT;
import com.magnetica.shared.racks.generic.ForwardEntityAs;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class MagneticaSettings {

	private static boolean clearCacheOnStartUp = false;
	private static boolean useCache;
	private static CommonCache cache;
	private static ForwardEntityAs forwardEntitiesAs = ForwardEntityAs.RESULT;
	private static boolean testModules = true;
	private static boolean printRunResults = true;

	public static void enableCache(boolean enable) {
		useCache = enable;
	}

	public static boolean useCache() {
		return useCache && cache != null;
	}

	public static void setCache(CommonCache cache) {
		MagneticaSettings.cache = cache;
	}

	public static CommonCache getCache() {
		return cache;
	}

	public static boolean clearCacheOnStartUp() {
		return clearCacheOnStartUp;
	}

	public static void clearCacheOnStartUp(boolean clearCacheOnStartUpAlways, boolean clearIfDevModeNotInProduction) {
		MagneticaSettings.clearCacheOnStartUp = clearCacheOnStartUpAlways
			|| (clearIfDevModeNotInProduction && !GWT.isProdMode()); //(!GWT.isProdMode() || GWT.getHostPageBaseURL().contains("localhost")));
	}

	public static String toStr() {
		return "MagneticaSettings(useCache=" + useCache() + "; clear cache on startup=" + clearCacheOnStartUp()
			+ "; cache=" + getCache() + ")";
	}

	/**
	 * @param result
	 */
	public static ForwardEntityAs standardEntityForwarding() {
		return MagneticaSettings.forwardEntitiesAs;
	}

	public static void standardEntityForwarding(ForwardEntityAs result) {
		MagneticaSettings.forwardEntitiesAs = result;
	}

	public static boolean testModules() {
		return testModules;
	}

	public static void testModules(boolean doTestModules) {
		testModules = doTestModules;
	}

	public static boolean printRunResults() {
		return printRunResults;
	}

	public static void printRunResults(boolean doPrintRunResults) {
		printRunResults = doPrintRunResults;
	}
}
