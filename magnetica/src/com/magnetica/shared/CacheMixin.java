/**
 * 
 */
package com.magnetica.shared;

import gwt.tools.shared.CommonCache;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class CacheMixin {

	private boolean use;

	public CacheMixin() {
		this(true);
	}

	public CacheMixin(boolean use) {
		use(use);
	}

	public boolean use() {
		return use && MagneticaSettings.useCache();
	}

	public void use(boolean use) {
		this.use = use;
	}

	public CommonCache get() {
		return MagneticaSettings.getCache();
	}

}
