/**
 * 
 */
package com.magnetica.shared.racks;

import gwt.tools.shared.Util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.DataElement;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.BasicModule;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 *            For convenience and type-safety, all forwarded entities will be
 *            converted to this type
 * 
 */
public abstract class ModuleRack<EntityType extends Entity, ModuleType extends Module<EntityType>> extends
	BasicModule<EntityType> {

	protected static final Statistics statistics = new Statistics();

	private List<ModuleType> modules;
	private Collection<Class<? extends DataElement>> dataElementsDependingOn;

	/**
	 * 
	 */
	public ModuleRack() {
	}

	public ModuleRack(List<ModuleType> modules, Collection<Class<? extends DataElement>> dataElementsDependingOn) {
		this.modules = modules;
		this.dataElementsDependingOn = dataElementsDependingOn;
	}

	/**
	 * @param modules2
	 */
	public ModuleRack(ModuleType... modules) {
		this.modules = Util.createList(modules);
	}

	// BEWARE: don't ever do stateful stuff!! modules are defined to be thread safe this allows us to run in a threaded, high performance environment, cf. appengine-web.xml: <threadsafe>true</threadsafe>.
	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.magnetica.shared.modules.Module#init()
	//	 */
	//	@Override public void init() {
	//		super.init();
	//		for (ModuleType module : getModules()) {
	//			module.init();
	//		}
	//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public final void run(Request<EntityType> request, Result<EntityType> resultSoFar, Callback callback) {
		if (request.isCancelled()) {
			return;
		}
		if (!timeIsUp(request)) {
			if (getModules().size() > 0) {
				runModules(request, resultSoFar, callback);
			} else {
				callback.success();
			}
		} else {
			callback.failure();
		}
	}

	protected abstract void runModules(Request<EntityType> request, Result<EntityType> resultSoFar, Callback callback);

	protected List<ModuleType> getModules() {
		return (modules == null) ? modules = new LinkedList<ModuleType>() : modules;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModuleImpl#testModule(com.magnetica
	 * .shared.modules.SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		boolean working = true;
		for (Module<?> module : getModules()) {
			working &= module.isWorking();
		}
		callback.done(working);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModule#cancel(com.magnetica.shared.
	 * data.Request, com.magnetica.shared.data.Result)
	 */
	@Override public void cancel(Request<EntityType> request, Result<EntityType> result) {
		if (getModules().size() > 0) {
			for (ModuleType module : getModules()) {
				module.cancel(request, result);
			}
		}
		super.cancel(request, result);
	}

	public static Statistics getStatistics() {
		return statistics.clone();
	}

	public static class Statistics {
		public int modulesRun;
		public int timeout;
		public int completed;
		public int cancelled;

		public Statistics clone() {
			Statistics clone = new Statistics();
			clone.modulesRun = modulesRun;
			clone.timeout = timeout;
			clone.completed = completed;
			clone.cancelled = cancelled;
			return clone;
		}
	}

	//	/*
	//	 * (non-Javadoc)
	//	 * 
	//	 * @see com.magnetica.shared.modules.Module#providesDataElements()
	//	 */
	//	@Override public Set<Class<? extends DataElement>> providesDataElements() {
	//		Set<Class<? extends DataElement>> provides = new HashSet<Class<? extends DataElement>>();
	//		for (Module module : getModules()) {
	//			provides.addAll(module.providesDataElements());
	//		}
	//		return provides;
	//	}
}
