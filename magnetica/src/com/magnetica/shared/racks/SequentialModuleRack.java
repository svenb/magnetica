/**
 * 
 */
package com.magnetica.shared.racks;

import gwt.tools.shared.Log;

import java.util.Iterator;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * A sequential rack will call each module in the order of the definition unless
 * one of the intermediate modules fails. E.g. if there are three modules A, B,
 * and C. B will be called after A finshed successfully. If B fails, C will
 * *NOT* be called at all.
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class SequentialModuleRack<EntityType extends Entity, ModuleType extends Module<EntityType>> extends
	ModuleRack<EntityType, ModuleType> {

	public SequentialModuleRack(ModuleType... modules) {
		super(modules);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ModuleRack#runModules(com.magnetica.shared
	 * .data.Request, com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override protected void runModules(Request<EntityType> request, Result<EntityType> resultSoFar, Callback callback) {
		runForNext(getModules().iterator(), request, resultSoFar, callback);
	}

	/**
	 * @param iterator
	 * @param callback
	 */
	private void runForNext(final Iterator<ModuleType> iterator, final Request<EntityType> request,
		final Result<EntityType> result, final Callback callback) {
		assert iterator != null;
		if (!iterator.hasNext() || timeIsUp(request)) {
			callback.success();
			return;
		}
		ModuleType module = iterator.next();
		// stop if this one is not working
		if (!module.isWorking()) {
			callback.failure();
			return;
		}
		Log.debug(this, ".runForNext: module, result", module, result);
		module.run(request, result, new Callback() {

			@Override public void success() {
				runForNext(iterator, request, result, callback);
			}

			@Override public void failure() {
				callback.failure();
			}
		});
	}

}
