/**
 * 
 */
package com.magnetica.shared.racks;

import java.util.Iterator;

import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class FailOverRack<EntityType extends Entity, ModuleType extends Module<EntityType>> extends
	ModuleRack<EntityType, ModuleType> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.racks.ModuleRack#runModules(com.magnetica.shared
	 * .data.Request, com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override protected void runModules(final Request<EntityType> request, final Result<EntityType> result,
		Callback callback) {
		runForNext(getModules().iterator(), request, result, callback);
	}

	/**
	 * @param iterator
	 * @param callback
	 */
	private void runForNext(final Iterator<ModuleType> iterator, final Request<EntityType> request,
		final Result<EntityType> result, final Callback callback) {
		assert iterator != null;
		if (!iterator.hasNext() || timeIsUp(request)) {
			callback.failure();
			return;
		}
		ModuleType module = iterator.next();
		if (!module.isWorking()) {
			runForNext(iterator, request, result, callback);
			return;
		}
		module.run(request, result, new Callback() {

			@Override public void success() {
				callback.success();
			}

			@Override public void failure() {
				runForNext(iterator, request, result, callback);
			}
		});
	}

}
