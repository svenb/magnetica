/**
 * 
 */
package com.magnetica.shared.racks.generic;

public enum ForwardEntityAs {
	REQUEST, RESULT;
}