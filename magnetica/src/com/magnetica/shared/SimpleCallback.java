/**
 * 
 */
package com.magnetica.shared;

import com.magnetica.shared.modules.generic.Callback;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class SimpleCallback implements Callback {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.Callback#success()
	 */
	@Override public void success() {
		done(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magnetica.shared.modules.Callback#failure()
	 */
	@Override public void failure() {
		done(false);
	}

	public abstract void done(boolean success);

}
