/**
 * 
 */
package com.magnetica.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public interface Client {

	void get(Request request, AsyncCallback<Result> callback);

}
