/**
 * 
 */
package com.magnetica.client.modules;

import gwt.tools.shared.Log;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.client.MagneticaService;
import com.magnetica.client.MagneticaServiceAsync;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.data.ServerResult;
import com.magnetica.shared.modules.generic.BasicModule;
import com.magnetica.shared.modules.generic.Callback;

/**
 * 
 * 
 * @author Sven Buschbeck
 * @param <EntityType>
 * 
 */
public class ServerConnection<EntityType extends Entity> extends BasicModule<EntityType> {

	private final MagneticaServiceAsync<EntityType> magneticaService = GWT.create(MagneticaService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public void run(Request<EntityType> request, final Result<EntityType> result, final Callback callback) {
		Log.debug(this, ".run: request,result", request, result);
		AsyncCallback<ServerResult<EntityType>> internalCallback = new AsyncCallback<ServerResult<EntityType>>() {

			@Override public void onSuccess(ServerResult<EntityType> serviceResult) {
				Log.debug(ServerConnection.class, ".run: call returned successfully", serviceResult.isSuccessful());
				if (result != null) {
					Log.debug(this, ".run: adopting service result", serviceResult.getResult());
					result.adopt(serviceResult.getResult());
				}
				if (serviceResult.isSuccessful() && serviceResult.getResult() != null) {
					callback.success();
				} else {
					callback.failure();
				}
			}

			@Override public void onFailure(Throwable caught) {
				System.out.println("########################\n");
				caught.printStackTrace();
				System.out.println("########################\n");
				//				Log.severe(
				//					ServerConnection.class,
				//					"magnetica.ServerConnection.run failed. Did you extend the MagneticaServiceImpl and add it to your web.xml? Duplicate the <url-pattern> line and change the service name (e.g. \"greet\" in the example) to \"magneticaservice\".");
				String generalHints = "To use magnetica, you need to do three things:\n"
					+ "* Make one of your services extend the magnetica service (i.e. replace \"extends RemoteServiceServlet\" with \" extends MagneticaServiceImpl\"\n"
					+ "* Add a <url-pattern> to the service to your web.xml (located in folder \"war/WEB-INF/\"). Most simple, duplicate the <url-pattern> of your service line and change the service name (e.g. \"greet\" in the example) to \"magneticaservice\".\n"
					+ "* Implement a constructor without any arguments in your sevice calling the super constructor (the one of the magnetica service you extend) to tell magnetica with module to start with on your server-side implementation.\n"
					+ "* Make sure that each object which is send or received from the server implements the interface java.io.Serializable (Embedded classes also if send/received separately!), has a constructor without parameters, and does not have parameters of type java.lang.Object. (GWT is picky about too generic parameters)";
				String message = "It seems magnetica has not been initialized properly, please refer to the hints below.";
				if (caught instanceof com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException) {
					message = "It's most likely that you did not extend MagneticaServiceImpl by the service you are using in your application.";
				} else if (caught == null) {
					message = "Did not receive any error which is odd. (caught==null)";
				} else if (caught instanceof com.google.gwt.user.client.rpc.SerializationException) {
					message = "One of the objects you tried to send or receive is not marked as Serializable (potential object: "
						+ caught.getMessage() + ")";
				} else if (caught.getMessage() == null) {
					message = "Did not receive any error description.";
				} else if (caught.getMessage().indexOf("<title>Error 404 NOT_FOUND</title>") >= 0) {
					message = "It's most likely that you did not add a <url-pattern> to your web.xml file.";
					try {
						// get suggestion from 404 page looking like this: <p>Problem accessing /com.eventmap.BackendTest/magneticaservice. Reason:
						final String before = "<p>Problem accessing ";
						final String after = ". Reason:";
						message += " In your case, it could be something like <url-pattern>"
							+ caught.getMessage().substring(caught.getMessage().indexOf(before) + before.length(),
								caught.getMessage().indexOf(after)) + "</url-pattern>.";
					} catch (Throwable t) {

					}
				} else if (caught.getMessage().indexOf("Caused by:</h3><pre>java.lang.InstantiationException") >= 0) {
					message = "It's most likely that you did not implement a constructor in your Service implementation.";
				}
				Log.severe(this, "Calling the magnetica service (server-side) failed:\n" + message
					+ " See details below.\n" + generalHints + "\n"
					+ "Technical info: magnetica.ServerConnection.run failed with following error: ");
				caught.printStackTrace();
				callback.failure();
			}
		};
		if (result == null) {
			Log.debug(this, ".run: call without preset result");
			magneticaService.run(request, internalCallback);
		} else {
			Log.debug(this, ".run: call with preset result");
			magneticaService.run(request, result, internalCallback);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModuleImp#testModule(com.magnetica.
	 * shared.modules.SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		try {
			run(null, null, callback);
		} catch (Throwable t) {
			callback.failure();
		}
	}

}
