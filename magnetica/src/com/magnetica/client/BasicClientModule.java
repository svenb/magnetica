/**
 * 
 */
package com.magnetica.client;

import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.BasicModule;
import com.magnetica.shared.modules.generic.Callback;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public abstract class BasicClientModule<EntityType extends Entity> extends BasicModule<EntityType> {

}
