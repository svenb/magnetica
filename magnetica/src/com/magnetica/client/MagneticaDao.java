/**
 * 
 */
package com.magnetica.client;

import gwt.tools.client.ClientCache;
import gwt.tools.shared.Log;
import gwt.tools.shared.Log.FormatOnDemand;

import com.magnetica.client.modules.ServerConnection;
import com.magnetica.shared.MagneticaSettings;
import com.magnetica.shared.SimpleCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.modules.generic.Callback;
import com.magnetica.shared.modules.generic.Module;

/**
 * 
 * 
 * @author Sven Buschbeck
 * 
 */
public class MagneticaDao<EntityType extends Entity> extends BasicClientModule<EntityType> {

	private static final boolean IGNORE_OLD_RESULT_DEFAULT = true;

	private final Module<EntityType> rootModule;
	private final boolean ignoreOldResult;
	private Request<EntityType> latestRequest;

	public MagneticaDao() {
		this(new ServerConnection<EntityType>(), IGNORE_OLD_RESULT_DEFAULT);
		MagneticaSettings.setCache(ClientCache.getInstance());

	}

	public MagneticaDao(boolean ignoreOldResult) {
		this(new ServerConnection<EntityType>(), ignoreOldResult);
	}

	public MagneticaDao(Module<EntityType> rootModule) {
		this(rootModule, IGNORE_OLD_RESULT_DEFAULT);
	}

	/**
	 * @param rootModule
	 */
	public MagneticaDao(Module<EntityType> rootModule, boolean ignoreOldResult) {
		this.rootModule = rootModule;
		this.ignoreOldResult = ignoreOldResult;
	}

	// useless: caller needs a ref to the result 
	//	public void run(final Request request, final Callback callback) {
	//		run(request, null, callback);
	//	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.Module#run(com.magnetica.shared.data.Request
	 * , com.magnetica.shared.data.Result,
	 * com.magnetica.shared.modules.Callback)
	 */
	@Override public void run(final Request<EntityType> request, final Result<EntityType> result,
		final Callback callback) {
		// TODO on ignoreOldResult: cancel all running request 
		assert callback != null;
		assert rootModule != null;
		final long start = System.currentTimeMillis();
		latestRequest = request;
		rootModule.run(request, result, new SimpleCallback() {

			@Override public void done(boolean success) {
				if (ignoreOldResult && request != latestRequest) {
					return;
				}
				printStats(request, result, start, success);
				if (success) {
					callback.success();
				} else {
					callback.failure();
				}
			}
		});
	}

	private void printStats(final Request<EntityType> request, final Result<EntityType> result, final long start,
		final boolean success) {
		Log.trace(MagneticaDao.class, "MagneticaDao.run: finished. stats", new FormatOnDemand() {

			@Override public String format() {
				return "Call finished in "
					+ (System.currentTimeMillis() - start)
					+ "ms "
					+ (success ? "successfully" : "but failed")
					+ ((result != null && result.getEntity() != null) ? ", #results=" + result.getEntity().size()
						: null)
					+ ". request="
					+ request
					+ "; result="
					+ result
					+ (MagneticaSettings.printRunResults() ? "; result.entity=" + result.getEntity().toString(true)
						: "");
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magnetica.shared.modules.BasicModuleImpl#testModule(com.magnetica
	 * .shared.modules.SimpleCallback)
	 */
	@Override protected void testModule(SimpleCallback callback) {
		assert rootModule != null;
		assert callback != null;
		if (rootModule.isWorking()) {
			callback.success();
		} else {
			callback.failure();
		}
	}

	/**
	 * 
	 */
	public void clearCache() {
		MagneticaSettings.getCache().clear();
	}
}
