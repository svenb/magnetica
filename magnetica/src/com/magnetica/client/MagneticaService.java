package com.magnetica.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.data.ServerResult;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("magneticaservice") public interface MagneticaService<EntityType extends Entity> extends
	RemoteService {
	ServerResult run(Request<EntityType> request);

	ServerResult run(Request<EntityType> request, Result<EntityType> result);
}
