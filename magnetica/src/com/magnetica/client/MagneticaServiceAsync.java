package com.magnetica.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.magnetica.shared.data.Entity;
import com.magnetica.shared.data.Request;
import com.magnetica.shared.data.Result;
import com.magnetica.shared.data.ServerResult;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface MagneticaServiceAsync<EntityType extends Entity> {

	void run(Request<EntityType> request, AsyncCallback<ServerResult<EntityType>> callback);

	void run(Request<EntityType> request, Result<EntityType> result, AsyncCallback<ServerResult<EntityType>> callback);
}
