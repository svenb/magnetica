= Concept =
There is a client and a server side. 

== Entities and DataElements ==
The basic information structure in magnetica are entities. Entities can contain a list of sub-entities and a set of DataElements.
The type of the DataElements defines the entity kind. Thus, when implementing DataElements, group the information semantically. 
E.g. for a person you would consider for example two DataElements: Person and Worker. Former defines a basic person e.g. by date of birth, name and such. The latter defines that this person has job giving, e.g. the company's name, job title and so on.

== Modules and Racks ==
Each is composed out of modules. All interfaces can be found in the shared section as well as some generic, reusable implementations executing modules in sequence or in parallel (called "racks" or "module racks").
The goal of modules and racks is to transform an incoming query into a tree of entities and attached metadata as a response to that query.

=== Example ===
Imagine you have a service that can retrieve information about workers in your company. The query contains the name of the worker.
Using of magnetica is especially help if the required information is distributed over several services, e.g. there is one service to retrieve the worker IDs by keyword (1) and another one to retrieve detailed information about a worker by ID (2).

So there are two tasks to be performed sequentially:
* Get ID by name (name forwarded to service (1) as keywords)
* Get worker details by ID (using service (2))

A suitable module/rack structure would be:
* SequentialModuleRack
** GetWorkersIdsByNameModule*
** ParallelDataRack
*** Get WorkerDetailsByIdModule*

Modules marked with * are your business logic implementations. Using racks also has the advantage that they handle caching for you!
GetWorkersIdsByNameModule would create one entity per found worker ID in the result set (which is given as parameter) with a WorkerIdDataElement (containing the ID) each.
Next there is the WorkerDetailsByIdModule which will be called by the parallel rack in parallel. Each call will give one worker in form of an entity to the module at a time, thus the job of this module is simply to get the WorkerIdDataElement from the entity, get the ID from it, call the service for worker details and return a filled WorkerMetaDataElement (containing job title and so on). Getting all the data together is handled by the framework.

= Architecture =

== Client ==
The client-side can be accessed using the MagneticaDao. If no parameter is given, it will automatically forward the call to the server. Giving a module allows to do additional computations on the client side already.

=== Configuration ===
Please refer to the server-side configuration section below. The client-side works the same besides that you might want to do the configuration in the onModuleLoad() method instead.

== Server ==

=== Adjusting web.xml ===
Make one of your servlets (e.g. the "GreetingServiceImpl" that comes with the example code) extend MagneticaServiceImpl. By doing so you can hand over your server-side base-module to magnetica. This module will be used by magnetica as the access to your server-side module graph calling it each time a request arrives from the client. 
Also, register the magnetica service in your web.xml (in folder /war/WEB-INF/):
Before:
 <servlet-mapping>
   <servlet-name>greetServlet</servlet-name>
   <url-pattern>/my_project/greet</url-pattern>
 </servlet-mapping>
After:
 <servlet-mapping>
   <servlet-name>greetServlet</servlet-name>
   <url-pattern>/my_project/greet</url-pattern>
   <url-pattern>/my_project/magneticaservice</url-pattern>
 </servlet-mapping>

=== Configure server-side magnetica ===
Create a static initializer in your service implementation class (e.g. "GreetingServiceImpl", see above) to configure magnetica specifically.
Example:
 static {
 		MagneticaSettings.enableCache(true);
 		MagneticaSettings.clearCacheOnStartUp(false);
 		System.out.println("GreetingServiceImpl.staticInit: configured magnetica: " + MagneticaSettings.toStr());
 	}
In this example the cache will be cleared each time the system starts up for testing purposes. In the end, the current settings are print to standard out.

= Implementing your Business Logic in Modules =
There are modules, which are supposed to do simple, atomic operations and racks which compose modules and execute then either sequentially or in parallel.

== Modules ==
For most of the modules you might want to create, you will find basic (comfort) version available (shared package) that take some effort out of it, e.g. 
* BasicModule,
** Some handles some self-testing and timeout handling
* BasicServerModule
* SimpleUrlRetrieverModule
** Supports loading external data in parallel (up to 10) using Google's UrlFetch API internall without any extra hassle for you
** When using this modules, please consider not to use the UrlFetch API yourself and the modules implementation in parallel as there is an *application-wide* limit of 10 parallel requests *max* and only one central use (implemented as part of the magnetica framework) can make sure this limit will hold. 
* DispatcherModule
** Calls different modules based on the given query metadata type (just add your mappings to the "mappings" map)

== Racks ==
* ParallelStructureRack
** Used to build up structures or entities in parallel
* ParallelDataRack
** Used to add MetaData instances to existing entities

== Evaluating If a Call Was Successful or Not ==
The major goal of magnetica is to handle error prone, slow, and unreliable services over the web and collect and merge the data found with in. 
In general if data does not load it does not mean that your module should call it a failure (i.e. returning via callback.failure()).
Why? Consider a service that returns information about an entity given it's ID. If this service does not know the ID you provided, the service will fail. But this is no failure for your module, because it understands that there is no such data for this entity using this service. Thus, returning a null or empty result is fine as this is all we can get to know using this service. As opposed, if the service call failed due to a timeout or network error surely callback.failure() is the best to do. In the end, the overall module structure will interpret "failure" as to retry next time, but if we know, that retrying will not make it any better (e.g. because this service does not respond properly to the given input data) retrying will be just a waste of bandwidth and time. 